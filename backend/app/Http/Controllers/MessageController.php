<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Message;
use Illuminate\Http\Request;
use App\Http\Requests\Messages\Store;
use App\Http\Requests\Messages\Destroy;

class MessageController extends Controller
{ 
   public function index(Request $request)
    {
        return $request->user()->received_messages; 
    }

    public function store(Store $request)
    {
        $message = Message::newMessage($request->validated(),$request->user()->id);
        return response()->json($message, 201);
    }

    public function destroy(Destroy $request, Message $message)
    {
        $message->delete();
        return response()->json(['message' => 'El mensaje fue eliminado con exito!']);
    }

    public function updateAndUnread(Request $request){
        if($request->message){
            $message = Message::where('id', '=', $request->message)->first();
            if($request->user()->id == $message->recipient_id){
                $message->update(['read' => true]);
            }
        }
            $unread = count($request->user()->unread_messages);
            return $unread;
    }
}
