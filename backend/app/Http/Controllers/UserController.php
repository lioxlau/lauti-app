<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Countries;
use App\Models\UserProfile;
use Illuminate\Http\Request;
use App\Http\Requests\Users\Destroy;
use App\Http\Requests\Users\Update;
use App\Http\Requests\Users\UpdateProfile;
use Illuminate\Http\JsonResponse;

class UserController extends Controller
{ 
    public function __construct()
    {
        $this->middleware('acl:users,full', ['only' => ['show', 'update', 'destroy']]);
        $this->middleware('acl:users,read_only,full', ['only' => ['index']]);
    }

    public function index(Request $request)
    {
        return new JsonResponse(User::filter($request->firstname, 
            $request->country_code, $request->sort_field, $request->sort_method));
    }

    public function show(User $user, Request $request)
    {
        return $user;
    }

    public function update(Update $request, User $user)
    {
        $user->update($request->validated());
        return $user;
    }

    public function destroy(Destroy $request, User $user)
    {
        $user->delete();
        return response()->json(['message' => 'El usuario fue eliminado con exito!']);
    }

    public function getCountries() 
    {
        return Countries::all();
    }

    public function emails() 
    {
        return User::emails();
    }

    public function updateProfile(UpdateProfile $request)
    {
        foreach ($request->validated()['users'] as $user) {
            User::where('id', $user['id'])->update(['user_profile_id' => $user['user_profile_id']]);
        }
        return response()->json(['message' => 'Se cambiaron los perfiles con exito']);
    }

}
