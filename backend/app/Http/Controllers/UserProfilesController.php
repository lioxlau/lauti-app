<?php

namespace App\Http\Controllers;

use App\Models\UserProfile;
use App\Models\Acl;
use App\Models\User;
use App\Models\AccessTypes;
use App\Models\Modules;
use App\Http\Requests\UserProfiles\Update;
use App\Http\Requests\UserProfiles\Store;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class UserProfilesController extends Controller
{
    public function __construct()
    {
        $this->middleware('acl:user-profiles,full', ['only' => ['show', 'update', 'destroy']]);
        $this->middleware('acl:user-profiles,read_only,full', ['only' => ['index']]);
    }

    public function index()
    {
        return UserProfile::with('acls')->get();
    }

    public function store(Store $request)
    {
        UserProfile::storeWithAcls($request->validated(), $request->acls);
        return response()->json('Perfil Creado con exito', 201);
    }

    public function show(UserProfile $userProfile)
    {
        $userProfile->acls;
        return $userProfile;
    }

    public function update(Update $request, UserProfile $userProfile)
    {
        $userProfile->update($request->validated());
        UserProfile::updateWithAcls($request->acls, $userProfile);
        return $userProfile;
    }

    public function destroy(UserProfile $userProfile)
    {
        return UserProfile::deleteWithAcls($userProfile);
    }

    public function modules()
    {
        return Modules::all();
    }

    public function accessTypes()
    {
        return AccessTypes::all();
    }

    public function users(UserProfile $userProfile)
    {
        return $userProfile->users;
    }
}
