<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\Users\Store;
use App\Http\Requests\Auth\Login;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use App\Models\User;
use App\Models\UserProfile;

class AuthController extends Controller
{

    /**
     * Create user
     *
     * @param  [string] firstname
     * @param  [string] email
     * @param  [string] password
     * @param  [string] password_confirmation
     * @return [string] message
     */
    public function signup(Store $request)
    {
        $highestLevel = UserProfile::highestLevel();
        $user = new User([
            'firstname' => $request->firstname,
            'lastname' => $request->lastname,
            'email' => $request->email,
            'password' => bcrypt($request->password),
            'country_code' => $request->country_code,
            'city' => $request->city,
            'user_profile_id' => $highestLevel->id
        ]);
        $user->save();
        return response()->json([
            'message' => 'Usuario creado con exito!'
        ], 201);
    }
  
    /**
     * Login user and create token
     *
     * @param  [string] email
     * @param  [string] password
     * @param  [boolean] remember_me
     * @return [string] access_token
     * @return [string] token_type
     * @return [string] expires_at
     */
    public function login(Login $request)
    {
        $credentials = request(['email', 'password']);
        if(!Auth::attempt($credentials))
            return response()->json([
                'message' => 'El mail y la contrasenia no coinciden'
            ], 401);
        $user = $request->user();
        $acl = $this->phraseAcl($user->userProfile->acls);
        $level = $user->userProfile->level;

        $tokenResult = $user->createToken('Personal Access Token'/*, $scopes*/);

        $token = $tokenResult->token;
        if ($request->remember_me)
            $token->expires_at = Carbon::now()->addWeeks(1);
        $token->save();
        return response()->json([
            'access_token' => $tokenResult->accessToken,
            'token_type' => 'Bearer',
            'expires_at' => Carbon::parse($tokenResult->token->expires_at)->toDateTimeString(),
            'acl' => $acl,
            'level' => $level
        ]);
    }
    /**
     * Logout user (Revoke the token)
     *
     * @return [string] message
     */
    public function logout(Request $request)
    {
        $request->user()->token()->revoke();
        return response()->json([
            'message' => 'Sesion cerrada con exito'
        ]);
    }
  
    /**
     * Get the authenticated User
     *
     * @return [json] user object
     */
    public function user(Request $request)
    {
        return response()->json($request->user());
    }    

    public function phraseAcl($acl)
    {
        $result = [];
        foreach ($acl as $access) {
            if ($access->access_type != 'locked') {
                array_push($result, $access->module . '-' . $access->access_type);
            }
        }
        return $result;
    }
}
