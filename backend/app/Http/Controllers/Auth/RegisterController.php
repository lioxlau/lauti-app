<?php

namespace App\Http\Controllers\Auth;

use App\Models\User;
use App\Models\UserProfile;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    use RegistersUsers;
    protected $redirectTo = '/home';

    public function __construct()
    {
        $this->middleware('guest');
    }

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:6', 'confirmed'],
            'firstname' => ['required', 'string'],
            'lastname' => ['required', 'string'],
            'country_code' => ['required', 'string'],
            'city' => ['required', 'string']
        ]);
    }

    protected function create(array $data)
    {
        $highestLevel = UserProfile::highestLevel();
        return User::create([
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            'firstname' => $data['firstname'],
            'lastname' => $data['lastname'],
            'country_code' => $data['country_code'],
            'city' => $data['city'],
            'user_profile_id' => $highestLevel->id
        ]);
    }
}
