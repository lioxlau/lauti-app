<?php

namespace App\Http\Requests\UserProfiles;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Auth\Access\AuthorizationException;

class Store extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.RegistrationRequest
     *
     * @return bool
     */
    public function authorize()
    {
        $authProfile = $this->user()->userProfile;
        $newLevel = $this->input('level');
        if(!$authProfile->canEdit($newLevel)){
            throw new AuthorizationException(__('validation.newlevel', ['authlevel' => $authProfile->level]));
        }
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|unique:user_profiles|max:64',
            'level' => 'required|integer|unique:user_profiles|max:10'
        ];
    }
    public function messages()
    {
        return [
            'name.required'  => 'Debe ingresar un nombre',
            'name.unique' => 'Ya existe un perfil con ese nombre',
            'level.required'  => 'Debe ingresar un nivel',
            'level.unique' => 'Ya hay un nivel con ese valor'
        ];
    }
}
