<?php

namespace App\Http\Requests\UserProfiles;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Auth\Access\AuthorizationException;
use App\Models\UserProfile;
use Illuminate\Validation\Rule;

class Update extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.RegistrationRequest
     *
     * @return bool
     */
    public function authorize()
    {
        $authProfile = $this->user()->userProfile;
        $currentLevel = $this->user_profile->level;
        $newLevel = $this->input('level');
        if(!$authProfile->canEdit($currentLevel)){
            throw new AuthorizationException(__('validation.currentlevel', ['authlevel' => $authProfile->level]));
        } elseif(!$authProfile->canEdit($newLevel)){
                    throw new AuthorizationException(__('validation.newlevel', ['authlevel' => $authProfile->level]));
        }
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['max:64', 'string', Rule::unique('user_profiles')->ignore($this->id)],
            'level' => ['max:10', 'integer', Rule::unique('user_profiles')->ignore($this->id)]
        ];
    }
    public function messages()
    {
        return [
            'name.unique' => 'Ya existe un perfil con ese nombre',
            'level.unique' => 'Ya hay un nivel con ese valor'
        ];
    }
}
