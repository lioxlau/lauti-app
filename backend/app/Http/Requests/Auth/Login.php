<?php

namespace App\Http\Requests\Auth;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class Login extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => [
                'required',
                'string',
                'email',
                'exists:users'
            ],
            'password' => [
                'required',
                'string'
            ],
            'remember_me' => 'boolean'
        ];
    }

    public function messages()
    {
        return [
            'password.required'  => 'Debe ingresar una contrasenia',
            'email.exists'  => 'El mail y contrasenia no coinciden',
            'email.required' => 'Debe ingresar su mail',
            'email.email' => 'Mail invalido',
            'email.unique' => 'Ese mail ya esta registrado'
        ];
    }
}
