<?php

namespace App\Http\Requests\Users;

use Illuminate\Foundation\Http\FormRequest;

class Store extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'firstname' => 'required|string',
            'lastname' => 'required|string',
            'email' => 'required|string|email|unique:users',
            'password' => 'required|string|confirmed',
            'country_code' => 'required|string',
            'city' => 'required|string'
        ];
    }
    public function messages()
    {
        return [
            'firstname.required' => 'Debe ingresar su nombre',
            'lastname.required' => 'Debe ingresar su apellido',
            'country_code.required' => 'Debe ingresar su pais',
            'city.required' => 'Debe ingresar su localidad',
            'email.required' => 'Debe ingresar su mail',
            'email.email' => 'Mail invalido',
            'email.unique' => 'Ese mail ya esta registrado',
            'password.required'  => 'Debe ingresar una contraseña',
            'password.confirmed'  => 'Las contraseñas no coinciden'
        ];
    }
}
