<?php

namespace App\Http\Requests\Users;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Illuminate\Auth\Access\AuthorizationException;
use App\Models\UserProfile;
use App\Models\User;

class UpdateProfile extends FormRequest
{

    public function authorize()
    {
        $this->authProfile = $this->user()->userProfile;
        $a = array_map(function ($u) {
            $currentlevel = User::find($u['id']);
            $newLevel = UserProfile::find($u['user_profile_id']);
            if($currentlevel && $newLevel) {
                if(!$this->authProfile->canEdit($currentlevel->userProfile->level)) {
                    throw new AuthorizationException(__('validation.currentlevel', ['authlevel' => $this->authProfile->level]));
                } elseif(!$this->authProfile->canEdit($newLevel->level)) {
                    throw new AuthorizationException(__('validation.newlevel', ['authlevel' => $this->authProfile->level]));
                }
            } else {
                return true;
            }
        }, $this->users);
        return true;
    }

    public function rules()
    {
        return [
            'users' => 'array',
            'users.*.id' => 'integer|distinct|exists:users|required',
            'users.*.user_profile_id' => 'integer|exists:user_profiles,id|required'
        ];
    }

    public function messages()
    {
        return [
            'users.array' => 'Error en el formato de la data (Array esperado)',
            'users.*.id.exists' => 'No se pudo encontrar a uno de los usuarios, pruebe denuevo',
            'users.*.user_profile_id.exists' => 'No se pudo encontrar a uno de los perfiles de usuarios, pruebe denuevo'
        ];
    }
}
