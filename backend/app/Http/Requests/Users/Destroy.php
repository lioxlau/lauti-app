<?php

namespace App\Http\Requests\Users;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Validation\Rule;
use App\Models\UserProfile;

class Destroy extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $authProfile = $this->user()->userProfile;
        $currentLevel = $this->user->userProfile->level;
        if(!$authProfile->canEdit($currentLevel) && ($this->user()->id != $this->user->id)){
            throw new AuthorizationException(__('validation.currentlevel', ['authlevel' => $authProfile->level]));
        } else {
            if($this->input('user_profile_id')){  
                $newLevel = UserProfile::find($this->input('user_profile_id'));
                if(!$authProfile->canEdit($newLevel->level)){
                    throw new AuthorizationException(__('validation.newlevel', ['authlevel' => $authProfile->level]));
                }       
            }
        return true;
        }   
    }

    public function rules()
    {
        return [];
    }

}