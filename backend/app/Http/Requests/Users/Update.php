<?php

namespace App\Http\Requests\Users;

use App\Http\Requests\Users\Destroy;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Validation\Rule;
use App\Models\UserProfile;

class Update extends Destroy
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'firstname' => 'string|max:64',
            'lastname' => 'string|max:64',
            'country_code' => 'string|max:64',
            'city' => 'string|max:64',
            'user_profile_id' => 'integer|exists:user_profiles,id',
            'email' => ['max:64', 'string', 'email', Rule::unique('users')->ignore($this->id)]
        ];
    }
    public function messages()
    {
        return [
            'email.email' => 'Mail invalido',
            'email.unique' => 'Ese mail ya esta registrado'
        ];
    }
}
