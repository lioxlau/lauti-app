<?php

namespace App\Http\Requests\Messages;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Auth\Access\AuthorizationException;

class Store extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.RegistrationRequest
     *
     * @return bool
     */
    public function authorize()
    {
        $userId = $this->user()->id;
        $recipientId = $this->input('recipient_id');
        if($userId == $recipientId){
            throw new AuthorizationException('No podes enviarse el mensaje a vos mismo.');            
        }
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'subject' => 'required|string|max:64',
            'message' => 'required|string|max:280',
            'recipient_id' => 'required|integer|exists:users,id'
        ];
    }
    public function messages()
    {
        return [
            'subject.required'  => 'Debe ingresar un Asunto',
            'message.required' => 'Debe ingresar un Mensaje',
            'recipient_id.required' => 'Debe ingresar un Receptor para el mensaje'
        ];
    }
}
