<?php

namespace App\Http\Requests\Messages;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Auth\Access\AuthorizationException;

class Destroy extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.RegistrationRequest
     *
     * @return bool
     */
    public function authorize()
    {
        $message = $this->message;
        $userId = $this->user()->id;
        if(!$message->sameId($userId)){
            throw new AuthorizationException('No puede moficiar un mensaje que no es suyo.');            
        }
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [];
    }
    public function messages() {
        return [];
    }
}