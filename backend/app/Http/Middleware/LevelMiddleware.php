<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\User;

class LevelMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $authLevel = User::getProfileLevel($request->user());
        $userLevel = User::getProfileLevel($user);
        if ($authLevel < $userLevel) {
            return $next($request);
        }
        return response()->json(['error' => 'You have not permission to access to the user information'])->setStatusCode(403);
    }
}

