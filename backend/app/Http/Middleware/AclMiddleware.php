<?php

namespace App\Http\Middleware;

use Closure;

class AclMiddleware 
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */

    public function handle($request, Closure $next, $module, $access1, $access2 = null)
    {
        if($request->user()->userProfile->checkAcl($module, $access1, $access2)){
            return $next($request);
        } else {
            return response()->json(['message' => 'Unauthorized'], 401);
        }
    } 
}