<?php

namespace App\Models;

use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Models\UserProfile;
use App\Models\Message;

class User extends Authenticatable
{

    use HasApiTokens, Notifiable;

    protected $fillable = [
        'password',
        'city',
        'firstname',
        'lastname',
        'email',
        'country_code',
        'user_profile_id'
    ];

    protected $hidden = [
        'password' //, 'remember_token',
    ];

    public function country() {
        return $this->belongsTo('App\Models\Countries', 'country_code', 'code')->select('code', 'name');
    }    

    public function userProfile() {
        return $this->belongsTo('App\Models\UserProfile')->select('id', 'name', 'level');
    }    

    public function received_messages() {
        return $this->hasMany('App\Models\Message', 'recipient_id')->with('sender','recipient')->orderBy('created_at', 'desc');
    }

    public function unread_messages() {
        return $this->hasMany('App\Models\Message', 'recipient_id')->where('read', "=", 0);
    }

    public static function emails() {
        return User::select('email', 'id')->get();
    }

    public static function filter(?string $firstname, 
            ?string $country_code, ?string $sort_field = null, ?string $sort_method = null) {
        $user = new User();
        $query = $user->newQuery();
        if ($firstname != null) {
            $query->where('firstname', 'like', "%$firstname%");
        }
        if ($country_code != null) {
            $query->where('country_code', $country_code);
        }
        if ($sort_field != null) {
            $query->orderBy($sort_field, $sort_method);
        }

        return $query->with('country', 'userProfile')->paginate(request('per_page'));
    }
}
