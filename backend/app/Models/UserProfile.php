<?php

namespace App\Models;
use App\Models\Acl;

use Illuminate\Database\Eloquent\Model;

class UserProfile extends Model
{

    protected $fillable = [
    	'id',
    	'name', 
    	'level'
    ];

    public function acls() {
        return $this->hasMany('App\Models\Acl', 'user_profile_id');
    }

    public function users() {
        return $this->hasMany('App\Models\User', 'user_profile_id');
    }

    public static function highestLevel() {
        $userProfile = UserProfile::where('level', UserProfile::max('level'))->first();
        return $userProfile;
    }

    public function canEdit($level) {
        return $this->level < $level;
    }


    public function checkAcl($module, $access1, $access2 = null){
    	$access = [];
		array_push($access, $access1, $access2);
    	return in_array(Acl::getAccess($this->id, $module)->access_type, $access);
    }

    public static function deleteWithAcls(UserProfile $userProfile) {
        $bool = ["has_users" => true];
        if(sizeof($userProfile->users) == 0){
            $userProfile->delete();
            $bool["has_users"] = false;
            return $bool;
        }
        return $bool;
    }

    public static function updateWithAcls($acls, $userProfile) {
        foreach ($acls as $action) {
            $row = Acl::getAccess($userProfile->id, $action['module']);
            $row->update([ 'access_type' => $action['access_type'] ]);
        }
    }

    public static function storeWithAcls($data, $acls) {
        $userProfile = UserProfile::create($data);
        foreach ($acls as $action) {
            $acl = [];
            $acl['user_profile_id'] = $userProfile->id; 
            $acl['module'] = $action['module'];
            $acl['access_type'] = $action['access_type'];
            Acl::create($acl);
        }
    }
}