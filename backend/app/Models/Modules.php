<?php 

namespace App\Models;

class Modules {

	const USERS = 'users';
	const USER_PROFILES = 'user-profiles';

	public static function all() {
		return [ self::USERS, self::USER_PROFILES];
	}
	
}