<?php 

namespace App\Models;

class AccessTypes {

	const LOCKED = [ 'code' => 'locked', 'name' => 'Sin acceso'];
	const READ_ONLY = [ 'code' => 'read_only', 'name' => 'Solo lectura'];
	const FULL_ACCESS = [ 'code' => 'full', 'name' => 'Acceso completo'];

	public static function all() {
		return [ self::LOCKED, self::READ_ONLY, self::FULL_ACCESS ];
	}
	
}