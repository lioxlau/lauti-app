<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class Acl extends Model
{
    protected $table = 'acl';
    protected $fillable = [
    	'user_profile_id', 
    	'module', 
    	'access_type'
    ];

    protected $primaryKey = [
    	'user_profile_id', 
    	'module'
    ];

    public $incrementing = false;

    public static function getAccess(Int $user_profile_id, $module) 
    {
    	return Acl::where('user_profile_id', $user_profile_id)->where('module', $module)->first();
    }

	/**
	 * Set the keys for a save update query.
	 *
	 * @param  \Illuminate\Database\Eloquent\Builder  $query
	 * @return \Illuminate\Database\Eloquent\Builder
	 */
	protected function setKeysForSaveQuery(Builder $query)
	{
	    $keys = $this->getKeyName();
	    if(!is_array($keys)){
	        return parent::setKeysForSaveQuery($query);
	    }

	    foreach($keys as $keyName){
	        $query->where($keyName, '=', $this->getKeyForSaveQuery($keyName));
	    }

	    return $query;
	}

	/**
	 * Get the primary key value for a save query.
	 *
	 * @param mixed $keyName
	 * @return mixed
	 */
	protected function getKeyForSaveQuery($keyName = null)
	{
	    if(is_null($keyName)){
	        $keyName = $this->getKeyName();
	    }

	    if (isset($this->original[$keyName])) {
	        return $this->original[$keyName];
	    }

	    return $this->getAttribute($keyName);
	}
    
    public function deleteAcl($user_profile_id)
    {
    	Alc::where('user_profile_id', $user_profile_id)->delete();
    }
}
