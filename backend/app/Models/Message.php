<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
	protected $fillable = [
        'id',
        'subject',
        'message',
        'read',
        'sender_id',
        'recipient_id'
    ];

    public function recipient() {
        return $this->belongsTo('App\Models\User', 'recipient_id');
    }  

    public function sender() {
        return $this->belongsTo('App\Models\User', 'sender_id');
    }

    public function sameId($id) {
        return $this->recipient_id == $id;
    }

    public static function newMessage($request, $user_id)
    {

    	$message = new Message([
            "subject" => $request['subject'],
	        "message" => $request['message'],
	        "read" => false,
	        "sender_id" => $user_id,
	        "recipient_id" => $request['recipient_id']
        ]);

        $message->save();

        return $message;
    }
}
