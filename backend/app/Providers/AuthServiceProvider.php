<?php

namespace App\Providers;

use Laravel\Passport\Passport;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        //

        Passport::routes();

        Passport::tokensCan([
            'acl-full' => 'Full access to ACL',
            'user-profiles-full' => 'Full access to user profiles',
            'users-full' => 'Full access to users',
            'acl-read_only' => 'Read only access to ACL',
            'user-profiles-read_only' => 'Read only access to user profiles',
            'users-read_only' => 'Read only access to users',
            'acl-locked' => 'Negated access to ACL',
            'user-profiles-locked' => 'Negated access to user profiles',
            'users-locked' => 'Negated access to users'
        ]);
    }
}
