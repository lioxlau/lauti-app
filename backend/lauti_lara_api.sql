-- MySQL dump 10.13  Distrib 5.6.31, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: lauti_lara_api
-- ------------------------------------------------------
-- Server version	5.6.31-0ubuntu0.14.04.2-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `acl`
--

DROP TABLE IF EXISTS `acl`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `acl` (
  `user_profile_id` int(10) unsigned NOT NULL,
  `module` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_type` enum('locked','read_only','full') COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`user_profile_id`,`module`),
  CONSTRAINT `acl_user_profile_id_foreign` FOREIGN KEY (`user_profile_id`) REFERENCES `user_profiles` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `acl`
--

LOCK TABLES `acl` WRITE;
/*!40000 ALTER TABLE `acl` DISABLE KEYS */;
INSERT INTO `acl` VALUES (1,'acl','full',NULL,NULL),(1,'user_profiles','full',NULL,NULL),(1,'users','full',NULL,NULL),(2,'acl','locked',NULL,NULL),(2,'user_profiles','read_only',NULL,NULL),(2,'users','full',NULL,NULL),(3,'acl','locked',NULL,NULL),(3,'user_profiles','locked',NULL,NULL),(3,'users','read_only',NULL,NULL);
/*!40000 ALTER TABLE `acl` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `countries`
--

DROP TABLE IF EXISTS `countries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `countries` (
  `code` varchar(3) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `countries`
--

LOCK TABLES `countries` WRITE;
/*!40000 ALTER TABLE `countries` DISABLE KEYS */;
INSERT INTO `countries` VALUES ('ABW','Aruba',NULL,NULL),('AFG','Afghanistan',NULL,NULL),('AGO','Angola',NULL,NULL),('AIA','Anguilla',NULL,NULL),('ALA','Åland Islands',NULL,NULL),('ALB','Albania',NULL,NULL),('AND','Andorra',NULL,NULL),('ARE','United Arab Emirates',NULL,NULL),('ARG','Argentina',NULL,NULL),('ARM','Armenia',NULL,NULL),('ASM','American Samoa',NULL,NULL),('ATA','Antarctica',NULL,NULL),('ATF','French Southern and Antarctic Lands',NULL,NULL),('ATG','Antigua and Barbuda',NULL,NULL),('AUS','Australia',NULL,NULL),('AUT','Austria',NULL,NULL),('AZE','Azerbaijan',NULL,NULL),('BDI','Burundi',NULL,NULL),('BEL','Belgium',NULL,NULL),('BEN','Benin',NULL,NULL),('BES','Caribbean Netherlands',NULL,NULL),('BFA','Burkina Faso',NULL,NULL),('BGD','Bangladesh',NULL,NULL),('BGR','Bulgaria',NULL,NULL),('BHR','Bahrain',NULL,NULL),('BHS','The Bahamas',NULL,NULL),('BIH','Bosnia and Herzegovina',NULL,NULL),('BLM','Saint Barthélemy',NULL,NULL),('BLR','Belarus',NULL,NULL),('BLZ','Belize',NULL,NULL),('BMU','Bermuda',NULL,NULL),('BOL','Bolivia',NULL,NULL),('BRA','Brazil',NULL,NULL),('BRB','Barbados',NULL,NULL),('BRN','Brunei',NULL,NULL),('BTN','Bhutan',NULL,NULL),('BVT','Bouvet Island',NULL,NULL),('BWA','Botswana',NULL,NULL),('CAF','Central African Republic',NULL,NULL),('CAN','Canada',NULL,NULL),('CCK','Cocos (Keeling) Islands',NULL,NULL),('CHE','Switzerland',NULL,NULL),('CHL','Chile',NULL,NULL),('CHN','China',NULL,NULL),('CIV','Ivory Coast',NULL,NULL),('CMR','Cameroon',NULL,NULL),('COD','Democratic Republic of the Congo',NULL,NULL),('COG','Republic of the Congo',NULL,NULL),('COK','Cook Islands',NULL,NULL),('COL','Colombia',NULL,NULL),('COM','Comoros',NULL,NULL),('CPV','Cabo Verde',NULL,NULL),('CRI','Costa Rica',NULL,NULL),('CUB','Cuba',NULL,NULL),('CUW','Curaçao',NULL,NULL),('CXR','Christmas Island',NULL,NULL),('CYM','Cayman Islands',NULL,NULL),('CYP','Cyprus',NULL,NULL),('CZE','Czech Republic',NULL,NULL),('DEU','Germany',NULL,NULL),('DJI','Djibouti',NULL,NULL),('DMA','Dominica',NULL,NULL),('DNK','Denmark',NULL,NULL),('DOM','Dominican Republic',NULL,NULL),('DZA','Algeria',NULL,NULL),('ECU','Ecuador',NULL,NULL),('EGY','Egypt',NULL,NULL),('ERI','Eritrea',NULL,NULL),('ESH','Western Sahara',NULL,NULL),('ESP','Spain',NULL,NULL),('EST','Estonia',NULL,NULL),('ETH','Ethiopia',NULL,NULL),('FIN','Finland',NULL,NULL),('FJI','Fiji',NULL,NULL),('FLK','Falkland Islands',NULL,NULL),('FRA','France',NULL,NULL),('FRO','Faroe Islands',NULL,NULL),('FSM','Federated States of Micronesia',NULL,NULL),('GAB','Gabon',NULL,NULL),('GBR','United Kingdom',NULL,NULL),('GEO','Georgia (country',NULL,NULL),('GGY','Guernsey',NULL,NULL),('GHA','Ghana',NULL,NULL),('GIB','Gibraltar',NULL,NULL),('GIN','Guinea',NULL,NULL),('GLP','Guadeloupe',NULL,NULL),('GMB','The Gambia',NULL,NULL),('GNB','Guinea-Bissau',NULL,NULL),('GNQ','Equatorial Guinea',NULL,NULL),('GRC','Greece',NULL,NULL),('GRD','Grenada',NULL,NULL),('GRL','Greenland',NULL,NULL),('GTM','Guatemala',NULL,NULL),('GUF','French Guiana',NULL,NULL),('GUM','Guam',NULL,NULL),('GUY','Guyana',NULL,NULL),('HKG','Hong Kong',NULL,NULL),('HMD','Heard Island and McDonald Islands',NULL,NULL),('HND','Honduras',NULL,NULL),('HRV','Croatia',NULL,NULL),('HTI','Haiti',NULL,NULL),('HUN','Hungary',NULL,NULL),('IDN','Indonesia',NULL,NULL),('IMN','Isle of Man',NULL,NULL),('IND','India',NULL,NULL),('IOT','British Indian Ocean Territory',NULL,NULL),('IRL','Republic of Ireland',NULL,NULL),('IRN','Iran',NULL,NULL),('IRQ','Iraq',NULL,NULL),('ISL','Iceland',NULL,NULL),('ISR','Israel',NULL,NULL),('ITA','Italy',NULL,NULL),('JAM','Jamaica',NULL,NULL),('JEY','Jersey',NULL,NULL),('JOR','Jordan',NULL,NULL),('JPN','Japan',NULL,NULL),('KAZ','Kazakhstan',NULL,NULL),('KEN','Kenya',NULL,NULL),('KGZ','Kyrgyzstan',NULL,NULL),('KHM','Cambodia',NULL,NULL),('KIR','Kiribati',NULL,NULL),('KNA','Saint Kitts and Nevis',NULL,NULL),('KOR','South Korea',NULL,NULL),('KWT','Kuwait',NULL,NULL),('LAO','Laos',NULL,NULL),('LBN','Lebanon',NULL,NULL),('LBR','Liberia',NULL,NULL),('LBY','Libya',NULL,NULL),('LCA','Saint Lucia',NULL,NULL),('LIE','Liechtenstein',NULL,NULL),('LKA','Sri Lanka',NULL,NULL),('LSO','Lesotho',NULL,NULL),('LTU','Lithuania',NULL,NULL),('LUX','Luxembourg',NULL,NULL),('LVA','Latvia',NULL,NULL),('MAC','Macau',NULL,NULL),('MAF','Collectivity of Saint Martin',NULL,NULL),('MAR','Morocco',NULL,NULL),('MCO','Monaco',NULL,NULL),('MDA','Moldova',NULL,NULL),('MDG','Madagascar',NULL,NULL),('MDV','Maldives',NULL,NULL),('MEX','Mexico',NULL,NULL),('MHL','Marshall Islands',NULL,NULL),('MKD','Republic of Macedonia',NULL,NULL),('MLI','Mali',NULL,NULL),('MLT','Malta',NULL,NULL),('MMR','Myanmar',NULL,NULL),('MNE','Montenegro',NULL,NULL),('MNG','Mongolia',NULL,NULL),('MNP','Northern Mariana Islands',NULL,NULL),('MOZ','Mozambique',NULL,NULL),('MRT','Mauritania',NULL,NULL),('MSR','Montserrat',NULL,NULL),('MTQ','Martinique',NULL,NULL),('MUS','Mauritius',NULL,NULL),('MWI','Malawi',NULL,NULL),('MYS','Malaysia',NULL,NULL),('MYT','Mayotte',NULL,NULL),('NAM','Namibia',NULL,NULL),('NCL','New Caledonia',NULL,NULL),('NER','Niger',NULL,NULL),('NFK','Norfolk Island',NULL,NULL),('NGA','Nigeria',NULL,NULL),('NIC','Nicaragua',NULL,NULL),('NIU','Niue',NULL,NULL),('NLD','Netherlands',NULL,NULL),('NOR','Norway',NULL,NULL),('NPL','Nepal',NULL,NULL),('NRU','Nauru',NULL,NULL),('NZL','New Zealand',NULL,NULL),('OMN','Oman',NULL,NULL),('PAK','Pakistan',NULL,NULL),('PAN','Panama',NULL,NULL),('PCN','Pitcairn Islands',NULL,NULL),('PER','Peru',NULL,NULL),('PHL','Philippines',NULL,NULL),('PLW','Palau',NULL,NULL),('PNG','Papua New Guinea',NULL,NULL),('POL','Poland',NULL,NULL),('PRI','Puerto Rico',NULL,NULL),('PRK','North Korea',NULL,NULL),('PRT','Portugal',NULL,NULL),('PRY','Paraguay',NULL,NULL),('PSE','State of Palestine',NULL,NULL),('PYF','French Polynesia',NULL,NULL),('QAT','Qatar',NULL,NULL),('REU','Réunion',NULL,NULL),('ROU','Romania',NULL,NULL),('RUS','Russia',NULL,NULL),('RWA','Rwanda',NULL,NULL),('SAU','Saudi Arabia',NULL,NULL),('SDN','Sudan',NULL,NULL),('SEN','Senegal',NULL,NULL),('SGP','Singapore',NULL,NULL),('SGS','South Georgia and the South Sandwich Islands',NULL,NULL),('SHN','Saint Helena, Ascension and Tristan da Cunha',NULL,NULL),('SJM','Svalbard and Jan Mayen',NULL,NULL),('SLB','Solomon Islands',NULL,NULL),('SLE','Sierra Leone',NULL,NULL),('SLV','El Salvador',NULL,NULL),('SMR','San Marino',NULL,NULL),('SOM','Somalia',NULL,NULL),('SPM','Saint Pierre and Miquelon',NULL,NULL),('SRB','Serbia',NULL,NULL),('SSD','South Sudan',NULL,NULL),('STP','São Tomé and Príncipe',NULL,NULL),('SUR','Suriname',NULL,NULL),('SVK','Slovakia',NULL,NULL),('SVN','Slovenia',NULL,NULL),('SWE','Sweden',NULL,NULL),('SWZ','Eswatini',NULL,NULL),('SXM','Sint Maarten',NULL,NULL),('SYC','Seychelles',NULL,NULL),('SYR','Syria',NULL,NULL),('TCA','Turks and Caicos Islands',NULL,NULL),('TCD','Chad',NULL,NULL),('TGO','Togo',NULL,NULL),('THA','Thailand',NULL,NULL),('TJK','Tajikistan',NULL,NULL),('TKL','Tokelau',NULL,NULL),('TKM','Turkmenistan',NULL,NULL),('TLS','East Timor',NULL,NULL),('TON','Tonga',NULL,NULL),('TTO','Trinidad and Tobago',NULL,NULL),('TUN','Tunisia',NULL,NULL),('TUR','Turkey',NULL,NULL),('TUV','Tuvalu',NULL,NULL),('TWN','Taiwan',NULL,NULL),('TZA','Tanzania',NULL,NULL),('UGA','Uganda',NULL,NULL),('UKR','Ukraine',NULL,NULL),('UMI','United States Minor Outlying Islands',NULL,NULL),('URY','Uruguay',NULL,NULL),('USA','United States',NULL,NULL),('UZB','Uzbekistan',NULL,NULL),('VAT','Vatican City',NULL,NULL),('VCT','Saint Vincent and the Grenadines',NULL,NULL),('VEN','Venezuela',NULL,NULL),('VGB','British Virgin Islands',NULL,NULL),('VIR','United States Virgin Islands',NULL,NULL),('VNM','Vietnam',NULL,NULL),('VUT','Vanuatu',NULL,NULL),('WLF','Wallis and Futuna',NULL,NULL),('WSM','Samoa',NULL,NULL),('YEM','Yemen',NULL,NULL),('ZAF','South Africa',NULL,NULL),('ZMB','Zambia',NULL,NULL),('ZWE','Zimbabwe',NULL,NULL);
/*!40000 ALTER TABLE `countries` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (21,'2014_10_12_100000_create_password_resets_table',1),(22,'2016_06_01_000001_create_oauth_auth_codes_table',1),(23,'2016_06_01_000002_create_oauth_access_tokens_table',1),(24,'2016_06_01_000003_create_oauth_refresh_tokens_table',1),(25,'2016_06_01_000004_create_oauth_clients_table',1),(26,'2016_06_01_000005_create_oauth_personal_access_clients_table',1),(27,'2019_02_25_132843_create_countries_table',1),(28,'2019_02_25_133220_create_user_profiles_table',1),(29,'2019_02_25_143211_create_users_table',1),(30,'2019_02_25_143234_create_acl_table',1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_access_tokens`
--

DROP TABLE IF EXISTS `oauth_access_tokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `client_id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_access_tokens_user_id_index` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_access_tokens`
--

LOCK TABLES `oauth_access_tokens` WRITE;
/*!40000 ALTER TABLE `oauth_access_tokens` DISABLE KEYS */;
INSERT INTO `oauth_access_tokens` VALUES ('08f854a192a72bdc175139dcd63507f129f7f798645d1a556497aa216477b1952d3c5a4eb39adbeb',4,1,'Personal Access Token','[]',1,'2019-03-27 21:50:27','2019-03-27 21:50:27','2020-03-27 18:50:27'),('2c400717ede79a88cf36fdca37edb41035b75b781fbf5263bbc1c2afa01b91c2396cc16e6e1229a8',1,1,'Personal Access Token','[]',1,'2019-03-28 19:14:54','2019-03-28 19:14:54','2020-03-28 16:14:54'),('300ed4916615e24f7da7f96b8630139c0dca83215fa9fd7e477b605668e76fdf7f71ce8ea75ca4d4',1,1,'Personal Access Token','[]',1,'2019-03-28 21:55:12','2019-03-28 21:55:12','2020-03-28 18:55:12'),('3f8a66da25a3408795ee413ea7e81be7f10ab56be23c51ef20ecd522d97c7570c2f423309fe84af2',1,1,'Personal Access Token','[]',1,'2019-03-28 16:38:50','2019-03-28 16:38:50','2020-03-28 13:38:50'),('46e2c426856a893e9b1c8d7c62f2d723a84ceac5b63e64e601e7039c4219a9f1969af4e3cd970fac',1,1,'Personal Access Token','[]',1,'2019-03-28 16:58:02','2019-03-28 16:58:02','2020-03-28 13:58:02'),('48efc7cb9ad53f69061cbcca2182b87e235f7427cb845128fc853e727a2adeded63b630d58e2d1f2',1,1,'Personal Access Token','[]',1,'2019-03-28 22:43:05','2019-03-28 22:43:05','2020-03-28 19:43:05'),('5303a6577354fc8ad7bd48a557ff78b3d824ead2c4790c71e7fce48b88f99fcbefda951f6cd58bd1',1,1,'Personal Access Token','[]',1,'2019-03-28 21:55:20','2019-03-28 21:55:20','2020-03-28 18:55:20'),('550b575a74d854c336638d093010c6a83a41497f44dfe2f2b53f8d1d169e984f179f4c3fa109a710',2,1,'Personal Access Token','[]',0,'2019-04-03 18:07:10','2019-04-03 18:07:10','2020-04-03 15:07:10'),('5cd109fe7103e1cd5f7dd824a6e1c3951f42930c5ab801cf463a747430f91cc42695066d7f58fbb4',4,1,'Personal Access Token','[]',1,'2019-03-27 21:54:31','2019-03-27 21:54:31','2020-03-27 18:54:31'),('816017c90ce5071757456ce0729fe39383825e90bd50d5f8f7df5f796d02caed9a660741f78d0938',1,1,'Personal Access Token','[]',1,'2019-03-29 21:25:57','2019-03-29 21:25:57','2020-03-29 18:25:57'),('92631ca534115163afb6c97e1ee59476c5ae40de0a4ee66c5f4b8085c0582937639b2402a05d8c43',1,1,'Personal Access Token','[]',1,'2019-04-03 18:06:53','2019-04-03 18:06:53','2020-04-03 15:06:53'),('a04e378fe525e2f5104d8b562b6427e65974995c9c2f5562d953686118a8698b379da4e34be66dd9',1,1,'Personal Access Token','[]',1,'2019-03-28 22:42:46','2019-03-28 22:42:46','2020-03-28 19:42:46'),('a521fcdb7f445a44d6d8826aca7026934ceb63024c45c431bce183a8d0c84acffe6206208568a3d3',4,1,'Personal Access Token','[]',1,'2019-03-27 21:51:46','2019-03-27 21:51:46','2020-03-27 18:51:46'),('c7113fb11b45f37250300985a47424f92d900b651024b6b7f1c63b61a6272f6f4e72b37ee8f4d5bb',2,1,'Personal Access Token','[]',1,'2019-03-29 21:25:03','2019-03-29 21:25:03','2020-03-29 18:25:03'),('d6b9a840dd874a202ec85ae4281f3fbd757c701f0866bd3904371128a7ce0008ee0fbd3d2e401c7a',1,1,'Personal Access Token','[]',1,'2019-03-28 18:02:46','2019-03-28 18:02:46','2020-03-28 15:02:46');
/*!40000 ALTER TABLE `oauth_access_tokens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_auth_codes`
--

DROP TABLE IF EXISTS `oauth_auth_codes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `client_id` int(10) unsigned NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_auth_codes`
--

LOCK TABLES `oauth_auth_codes` WRITE;
/*!40000 ALTER TABLE `oauth_auth_codes` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_auth_codes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_clients`
--

DROP TABLE IF EXISTS `oauth_clients`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_clients` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_clients_user_id_index` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_clients`
--

LOCK TABLES `oauth_clients` WRITE;
/*!40000 ALTER TABLE `oauth_clients` DISABLE KEYS */;
INSERT INTO `oauth_clients` VALUES (1,NULL,'Laravel Personal Access Client','W8TETZVja6msm716nkhGk4JXkdAwKID6CbJQ0obn','http://localhost',1,0,0,'2019-03-27 21:50:17','2019-03-27 21:50:17');
/*!40000 ALTER TABLE `oauth_clients` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_personal_access_clients`
--

DROP TABLE IF EXISTS `oauth_personal_access_clients`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_personal_access_clients` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `client_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_personal_access_clients_client_id_index` (`client_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_personal_access_clients`
--

LOCK TABLES `oauth_personal_access_clients` WRITE;
/*!40000 ALTER TABLE `oauth_personal_access_clients` DISABLE KEYS */;
INSERT INTO `oauth_personal_access_clients` VALUES (1,1,'2019-03-27 21:50:17','2019-03-27 21:50:17');
/*!40000 ALTER TABLE `oauth_personal_access_clients` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_refresh_tokens`
--

DROP TABLE IF EXISTS `oauth_refresh_tokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_refresh_tokens`
--

LOCK TABLES `oauth_refresh_tokens` WRITE;
/*!40000 ALTER TABLE `oauth_refresh_tokens` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_refresh_tokens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_profiles`
--

DROP TABLE IF EXISTS `user_profiles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_profiles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `level` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_profiles`
--

LOCK TABLES `user_profiles` WRITE;
/*!40000 ALTER TABLE `user_profiles` DISABLE KEYS */;
INSERT INTO `user_profiles` VALUES (1,'Administrador',0,NULL,NULL),(2,'Moderador',1,NULL,NULL),(3,'Usuario',2,NULL,NULL);
/*!40000 ALTER TABLE `user_profiles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `city` varchar(64) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `firstname` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lastname` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `country_code` varchar(3) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_profile_id` int(10) unsigned NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`),
  KEY `users_user_profile_id_foreign` (`user_profile_id`),
  KEY `users_country_code_foreign` (`country_code`),
  CONSTRAINT `users_country_code_foreign` FOREIGN KEY (`country_code`) REFERENCES `countries` (`code`),
  CONSTRAINT `users_user_profile_id_foreign` FOREIGN KEY (`user_profile_id`) REFERENCES `user_profiles` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'lauti@lauti.com','$2y$10$TEF4rWtJYE4iWgmo3lBTWeXFGrM.JqjrvsW7nyU3Zq17e9Hjet3te','CABA','Lautaro','Martinez','ARG',1,NULL,NULL,NULL,'2019-04-03 18:18:02'),(2,'mauro@mauro.com','$2y$10$kBMQcqA2Hfb1kHs.TRQ79O0i21wKM6X0TcKu/wo6QC7ipic4IDIUu','Quilmes','Mauricio','Moralez','ARG',1,NULL,NULL,NULL,'2019-04-03 18:07:20');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-04-03 12:27:24
