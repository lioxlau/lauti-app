<?php

namespace Tests;

use Illuminate\Foundation\Testing\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;

    const USERNAME = 'lauti@lauti.com';
    const PASSWORD = 'elrbsest';

    protected $token_type;
    protected $access_token;

    public function getSeeders() {
    	return [];
    }

    public function setUp() {
        parent::setUp();

        $this->artisan('passport:client', ['--personal' => null, '--name' => 'Laravel Personal Access Client' ]);

        foreach ($this->getSeeders() as $seeder) {
            $this->artisan('db:seed', ['--class' => $seeder, '--database' => env('DB_CONNECTION') ]);
        }

        $this->login();
    }

    protected function login() {
        $response = $this->json('POST', '/api/auth/login', [
            'email' => self::USERNAME, 
            'password' => self::PASSWORD,
            'remember_me' => true
        ]);

        $response = json_decode($response->content());

        $this->token_type = $response->token_type;
        $this->access_token = $response->access_token;
        
        return $response;
    }

    protected function withoutLogin() {
        $this->token_type = null;
        $this->access_token = null;
    }

    public function json($method, $uri, array $data = [], array $headers = []) {
        if ($this->access_token) {
            $headers['authorization'] = $this->token_type . ' ' . $this->access_token;    
        }
        return parent::json($method, $uri, $data, $headers);
    }

}
