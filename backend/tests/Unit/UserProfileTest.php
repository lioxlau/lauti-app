<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\JsonResponse;

class UserProfileTest extends TestCase
{
	use \Illuminate\Foundation\Testing\DatabaseMigrations;

    public function getSeeders() 
    {
    	return ['CountriesTableSeeder', 'UserProfilesTableSeeder', 'UsersTableSeeder', 'AclTableSeeder'];
    }

    public function testCreateUserProfileOK()
    {
    	$response = $this->json('POST', '/api/user-profiles', [
			'level' => '10',
			'name' => 'TEST_PROFILE'
		]);
	    $this->assertEquals(201, $response->status());
    }

    public function testDeleteUserProfileOK()
    {
    	$response = $this->json('DELETE', '/api/user-profiles/4');
	    $this->assertEquals(200, $response->status());
    }

    public function testACLUnauthorizedEditUserProfile()
    {
        $this->withoutLogin();

        $response = $this->json('POST', '/api/auth/login', [
            'email' => 'test@test.com', 
            'password' => 'elrbsest',
            'remember_me' => true
        ]);

        $response = json_decode($response->content());
        $this->assertEquals('Bearer', $response->token_type);

        $response_put = $this->json('PUT', '/api/user-profiles/1', [
            'level' => '99',
            'name' => 'EDIT_PROFILE'
        ]);

        $this->assertEquals(401, $response_put->status());
    }

    public function testACLUnauthorizedEditAdminProfile()
    {
        $this->withoutLogin();

        $response = $this->json('POST', '/api/auth/login', [
            'email' => 'mauro@mauro.com', 
            'password' => 'elrbsest',
            'remember_me' => true
        ]);

        $response = json_decode($response->content());
        $this->assertEquals('Bearer', $response->token_type);

        $response_put = $this->json('PUT', '/api/user-profiles/1', [
            'level' => '99',
            'name' => 'EDIT_PROFILE'
        ]);

        $this->assertEquals(401, $response_put->status());
    }

}