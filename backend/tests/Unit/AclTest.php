<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\JsonResponse;

class UserTest extends TestCase
{
	use \Illuminate\Foundation\Testing\DatabaseMigrations;

    public function getSeeders() 
    {
    	return ['CountriesTableSeeder', 'UserProfilesTableSeeder', 'UsersTableSeeder', 'AclTableSeeder'];
    }

}