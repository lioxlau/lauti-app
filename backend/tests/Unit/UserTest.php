<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\JsonResponse;

class UserTest extends TestCase
{
	use \Illuminate\Foundation\Testing\DatabaseMigrations;

    public function getSeeders() 
    {
    	return ['CountriesTableSeeder', 'UserProfilesTableSeeder', 'UsersTableSeeder', 'AclTableSeeder'];
    }

    public function testCreateUser()
    {
    	$this->withoutLogin();

    	$response = $this->json('POST', '/api/auth/signup', [
			'email' => "USER@USER.com",
			'city' => "USER",
			'firstname' => "USER",
			'lastname' => "USER",
			'country_code' => "ARG",
			'password' => 'elrbsest',
			'password_confirmation' => 'elrbsest'
		]);

	    $this->assertEquals(201, $response->status());
    }
    
    public function testLogin() 
    {
    	$this->withoutLogin();

	    $response = $this->json('POST', '/api/auth/login', [
	    	'email' => 'lauti@lauti.com', 
			'password' => 'elrbsest',
			'remember_me' => true
	    ]);

	    $response = json_decode($response->content());
	    $this->assertEquals('Bearer', $response->token_type);
    }

    public function testDeleteUser()
    {
    	$response = $this->json('DELETE', '/api/user/4');
	    $this->assertEquals(200, $response->status());
    }

	public function testGETUserList()
	{
		$response = $this->json('GET', '/api/user/');
	    $this->assertEquals(200, $response->status());
	}

	public function testChangeProfile()
	{
		$response_put = $this->json('PUT', '/api/user/1', [
			'id' => 1,
			'email' => "lauti@lauti.com",
			'city' => "CABA",
			'firstname' => "Lautaro",
			'lastname' => "Martinez",
			'country_code' => "ARG",
			'user_profile_id' => 3,
		]);

		$this->assertEquals(200, $response_put->status());

		$this->assertDatabaseHas('users', [
			'id' => 1,
			'email' => "lauti@lauti.com",
			'city' => "CABA",
			'firstname' => "Lautaro",
			'lastname' => "Martinez",
			'country_code' => "ARG",
			'user_profile_id' => 3
		]);
	}

	public function testCanNotChangeID()
	{
		$response_put = $this->json('PUT', '/api/user/1', [
			'id' => 2,
			'email' => "lauti@lauti.com",
			'city' => "CABA",
			'firstname' => "Lautaro",
			'lastname' => "Martinez",
			'country_code' => "ARG",
			'user_profile_id' => 1,
		]);

		$this->assertEquals(422, $response_put->status());
	}

	public function testACLUnauthorizedEditUser()
	{
		$this->withoutLogin();

	    $response = $this->json('POST', '/api/auth/login', [
	    	'email' => 'test@test.com', 
			'password' => 'elrbsest',
			'remember_me' => true
	    ]);

	    $response = json_decode($response->content());
	    $this->assertEquals('Bearer', $response->token_type);

	    $response_put = $this->json('PUT', '/api/user/1', [
			'id' => 1,
			'email' => "EDITADO@EDITADO.com",
			'city' => "EDITADO",
			'firstname' => "EDITADO",
			'lastname' => "EDITADO",
			'country_code' => "ARG",
			'user_profile_id' => 1
		]);

		$this->assertEquals(401, $response_put->status());
	}

	public function testACLUnauthorizedEditAdmin()
	{
		$this->withoutLogin();

	    $response = $this->json('POST', '/api/auth/login', [
	    	'email' => 'mauro@mauro.com', 
			'password' => 'elrbsest',
			'remember_me' => true
	    ]);

	    $response = json_decode($response->content());
	    $this->assertEquals('Bearer', $response->token_type);

	    $response_put = $this->json('PUT', '/api/user/1', [
			'id' => 1,
			'email' => "EDITADO@EDITADO.com",
			'city' => "EDITADO",
			'firstname' => "EDITADO",
			'lastname' => "EDITADO",
			'country_code' => "ARG",
			'user_profile_id' => 1
		]);

		$this->assertEquals(401, $response_put->status());
	}

	public function testCreateExistingUser()
    {
    	$this->withoutLogin();

    	$response = $this->json('POST', '/api/auth/signup', [
			'email' => "lauti@lauti.com",
			'city' => "USER",
			'firstname' => "USER",
			'lastname' => "USER",
			'country_code' => "ARG",
			'password' => 'elrbsest',
			'password_confirmation' => 'elrbsest'
		]);
	    $response->assertSeeText("Ese mail ya esta registrado");
    }
}