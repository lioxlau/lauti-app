<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group([
    'prefix' => 'auth'
], function () {
    Route::post('login', 'AuthController@login');
    Route::post('signup', 'AuthController@signup');
  
    Route::group([
      'middleware' => 'auth:api'
    ], function() {
        Route::get('logout', 'AuthController@logout');
        Route::get('user', 'AuthController@user');
    });
});

Route::get('/countries', 'UserController@getCountries');

#TODO: https://laravel.com/docs/5.8/passport#assigning-scopes-to-tokens
Route::group([
  'middleware' => 'auth:api'
], function() {

    Route::apiResource('message', 'MessageController')->except('update','show');
    Route::apiResource('user', 'UserController')->except('store');
    Route::apiResource('user-profiles', 'UserProfilesController');

	Route::get('/modules', 'UserProfilesController@modules');
	Route::get('/access-types', 'UserProfilesController@accessTypes');
    Route::get('/unread', 'MessageController@updateAndUnread');
    Route::get('/emails', 'UserController@emails');
    Route::get('/user-profiles/users/{user_profile}', 'UserProfilesController@users');
    Route::post('/user/update-profile', 'UserController@updateProfile');

});