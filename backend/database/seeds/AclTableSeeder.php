<?php

use Illuminate\Database\Seeder;

class AclTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('acl')->insert(['user_profile_id' => 1,'module' => 'users','access_type' => 'full']);
    	DB::table('acl')->insert(['user_profile_id' => 1,'module' => 'user-profiles','access_type' => 'full']);

    	DB::table('acl')->insert(['user_profile_id' => 2,'module' => 'users','access_type' => 'full']);
    	DB::table('acl')->insert(['user_profile_id' => 2,'module' => 'user-profiles','access_type' => 'read_only']);

    	DB::table('acl')->insert(['user_profile_id' => 3,'module' => 'users','access_type' => 'read_only']);
    	DB::table('acl')->insert(['user_profile_id' => 3,'module' => 'user-profiles','access_type' => 'locked']);

        DB::table('acl')->insert(['user_profile_id' => 4,'module' => 'users','access_type' => 'locked']);
        DB::table('acl')->insert(['user_profile_id' => 4,'module' => 'user-profiles','access_type' => 'locked']);
    }
}
