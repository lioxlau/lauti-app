<?php

use Illuminate\Database\Seeder;

class MessagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('messages')->insert([
            'id' => '1',
			'subject' => 'Hola Mauro',
			'message' => 'Hola Mauro este es un mensaje de texto',
			'read' => false,
			'sender_id' => '1',
			'recipient_id' => '2'
        ]);

        DB::table('messages')->insert([
            'id' => '2',
			'subject' => 'Hola Lautaro',
			'message' => 'Hola Lautaro este es un mensaje de texto',
			'read' => false,
			'sender_id' => '2',
			'recipient_id' => '1'
        ]);
    }
}
