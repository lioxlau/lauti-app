<?php

use Illuminate\Database\Seeder;

class UserProfilesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         DB::table('user_profiles')->insert([
            'id' => 1,
            'name' => 'Administrador',
            'level' => 0
        ]);

        DB::table('user_profiles')->insert([
            'id' => 2,
            'name' => 'Moderador',
            'level' => 1
        ]);

        DB::table('user_profiles')->insert([
            'id' => 3,
            'name' => 'Usuario',
            'level' => 2
        ]);

         DB::table('user_profiles')->insert([
            'id' => 4,
            'name' => 'TestProfile',
            'level' => 3
        ]);
    }
}
