<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'id' => 1,
            'password' => bcrypt('elrbsest'),
            'city' => 'CABA',
            'firstname' => 'Lautaro',
            'lastname' => 'Martinez',
            'email' => 'lauti@lauti.com',
            'country_code' => 'ARG',
            'user_profile_id' => 1
        ]);

        DB::table('users')->insert([
            'id' => 2,
            'password' => bcrypt('elrbsest'),
            'city' => 'Quilmes',
            'firstname' => 'Mauricio',
            'lastname' => 'Moralez',
            'email' => 'mauro@mauro.com',
            'country_code' => 'ARG',
            'user_profile_id' => 2
        ]);

        DB::table('users')->insert([
            'id' => 3,
            'password' => bcrypt('elrbsest'),
            'city' => 'La Plata',
            'firstname' => 'Pepe',
            'lastname' => 'Argento',
            'email' => 'pepe@pepe.com',
            'country_code' => 'ARG',
            'user_profile_id' => 3
        ]);

        DB::table('users')->insert([
            'id' => 4,
            'password' => bcrypt('elrbsest'),
            'city' => 'test',
            'firstname' => 'TestFN',
            'lastname' => 'TestLN',
            'email' => 'test@test.com',
            'country_code' => 'BRA',
            'user_profile_id' => 4
        ]);

        DB::table('users')->insert([
            'id' => 5,
            'password' => bcrypt('vDV6C39Ck'),
            'city' => 'Trondheim',
            'firstname' => 'Guinna',
            'lastname' => 'Moncrefe',
            'email' => 'gmoncrefe4@seesaa.net',
            'country_code' => 'ZWE',
            'user_profile_id' => 3
        ]);

        DB::table('users')->insert([
            'id' => 6,
            'password' => bcrypt('1Syg9RPgn'),
            'city' => 'Liulin',
            'firstname' => 'Tobe',
            'lastname' => 'Hickin',
            'email' => 'thickin5@opensource.org',
            'country_code' => 'TUR',
            'user_profile_id' => 3
        ]);

        DB::table('users')->insert([
            'id' => 7,
            'password' => bcrypt('nkTU9osvj2YF'),
            'city' => 'Neyvo-Rudyanka',
            'firstname' => 'Odo',
            'lastname' => 'Fensome',
            'email' => 'ofensome6@reddit.com',
            'country_code' => 'TUR',
            'user_profile_id' => 3
        ]);

        DB::table('users')->insert([
            'id' => 8,
            'password' => bcrypt('FI3hma2scn'),
            'city' => 'Johogunung',
            'firstname' => 'Martynne',
            'lastname' => 'Ballsdon',
            'email' => 'mballsdon7@thetimes.com',
            'country_code'=> 'SSD',
            'user_profile_id' => 3
        ]);

            DB::table('users')->insert([
            'id' => 9,
            'password' => bcrypt('q0WSSAF'),
            'city' => 'Besole',
            'firstname' => 'Lezley',
            'lastname' => 'Leeke',
            'email' => 'lleeke8@spotify.com',
            'country_code' => 'PHL',
            'user_profile_id' => 3
        ]);

        DB::table('users')->insert([
            'id' => 10,
            'password' => bcrypt('Oxa0B73'),
            'city' => 'Spitak',
            'firstname' => 'Jasen',
            'lastname' => 'Darracott',
            'email' => 'jdarracott9@time.com',
            'country_code' => 'MTQ',
            'user_profile_id' => 3
        ]);

            DB::table('users')->insert([
            'id' => 11,
            'password' => bcrypt('Z5tMHxe6'),
            'city' => 'Ústí nad Labem',
            'firstname' => 'Yorgo',
            'lastname' => 'Hamsley',
            'email' => 'yhamsleya@cargocollective.com',
            'country_code' => 'MTQ',
            'user_profile_id' => 3
        ]);

        DB::table('users')->insert([
            'id' => 12,
            'password' => bcrypt('gAB9yNW'),
            'city' => 'Stockholm',
            'firstname' => 'Oneida',
            'lastname' => 'Sweed',
            'email' => 'osweedb@springer.com',
            'country_code' => 'LIE',
            'user_profile_id' => 3
        ]);

            DB::table('users')->insert([
            'id' => 13,
            'password' => bcrypt('QJeYNmNR9'),
            'city' => 'Bolaoit',
            'firstname' => 'Corty',
            'lastname' => 'Crunkhurn',
            'email' => 'ccrunkhurnc@senate.gov',
            'country_code' => 'IRQ',
            'user_profile_id' => 3
        ]);

            DB::table('users')->insert([
            'id' => 14,
            'password' => bcrypt('fJquHSxDvTQ'),
            'city' => 'Gardutanjak',
            'firstname' => 'Baily',
            'lastname' => 'Hanster',
            'email' => 'bhansterd@walmart.com',
            'country_code' => 'GUF',
            'user_profile_id' => 3
        ]);

            DB::table('users')->insert([
            'id' => 15,
            'password' => bcrypt('5dT60N'),
            'city' => 'Jezerce',
            'firstname' => 'Flin',
            'lastname' => 'Marrow',
            'email' => 'fmarrowe@narod.ru',
            'country_code' => 'GUF',
            'user_profile_id' => 3
        ]);

            DB::table('users')->insert([
            'id' => 16,
            'password' => bcrypt('cDApalH'),
            'city' => 'Sembabule',
            'firstname' => 'Kearney',
            'lastname' => 'Dytham',
            'email' => 'kdythamf@ning.com',
            'country_code' => 'GTM',
            'user_profile_id' => 3
        ]);

            DB::table('users')->insert([
            'id' => 17,
            'password' => bcrypt('Bokty8aMvw'),
            'city' => 'Rokytne',
            'firstname' => 'Hanson',
            'lastname' => 'Rogan',
            'email' => 'hrogang@jugem.jp',
            'country_code' => 'FJI',
            'user_profile_id' => 3
        ]);

            DB::table('users')->insert([
            'id' => 18,
            'password' => bcrypt('6bGdJMMqy'),
            'city' => 'Guaduas',
            'firstname' => 'Halsy',
            'lastname' => 'Labin',
            'email' => 'hlabinh@last.fm',
            'country_code' => 'FIN',
            'user_profile_id' => 3
        ]);

            DB::table('users')->insert([
            'id' => 19,
            'password' => bcrypt('kFZsisXNZqN'),
            'city' => 'Karangpapak',
            'firstname' => 'Lucias',
            'lastname' => 'Elkins',
            'email' => 'lelkinsi@netlog.com',
            'country_code' => 'CUW',
            'user_profile_id' => 3
        ]);

            DB::table('users')->insert([
            'id' => 20,
            'password' => bcrypt('Rx1S4FuhRyr'),
            'city' => 'Batanghari',
            'firstname' => 'Cherise',
            'lastname' => 'Pendlenton',
            'email' => 'cpendlentonj@amazon.com',
            'country_code' => 'BTN',
            'user_profile_id' => 3
        ]);

         DB::table('users')->insert([
            'id' => 21,
            'password' => bcrypt('G4OBewe'),
            'city' => 'Qasr Abu Hadi',
            'firstname' => 'Alameda',
            'lastname' => 'Waszkiewicz',
            'email' => 'awaszkiewicz0@stanford.edu',
            'country_code' => 'BRA',
            'user_profile_id' => 3
        ]);

        DB::table('users')->insert([
            'id' => 22,
            'password' => bcrypt('xF9LGFy'),
            'city' => 'Dubna',
            'firstname' => 'Maury',
            'lastname' => 'Turtle',
            'email' => 'mturtle1@canalblog.com',
            'country_code' => 'BRA',
            'user_profile_id' => 3
        ]);

        DB::table('users')->insert([
            'id' => 23,
            'password' => bcrypt('DHCfHLL3HJ'),
            'city' => 'Žiželice',
            'firstname' => 'Jerrylee',
            'lastname' => 'Swindall',
            'email' => 'jswindall2@nps.gov',
            'country_code' => 'BRA',
            'user_profile_id' => 3
        ]);

        DB::table('users')->insert([
            'id' => 24,
            'password' => bcrypt('0DFpye18Cwgj'),
            'city' => 'Columbus',
            'firstname' => 'Foster',
            'lastname' => 'Crutchley',
            'email' => 'fcrutchley3@state.gov',
            'country_code' => 'ABW',
            'user_profile_id' => 3
        ]);
    }
}