<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAclTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('acl', function (Blueprint $table) {
            $table->unsignedInteger('user_profile_id');
            $table->string('module', 64);
            $table->enum('access_type', ['locked','read_only','full']); 
            $table->primary(['user_profile_id', 'module']);

            //$table->dropForeign('user_profile_id_foreign');
            $table->foreign('user_profile_id')->references('id')->on('user_profiles')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('acl');
    }
}
