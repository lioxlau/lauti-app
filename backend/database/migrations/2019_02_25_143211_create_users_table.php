<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('email', 64)->unique();
            $table->string('password', 64);
            $table->string('city', 64)->nullable();
            $table->string('firstname', 64);
            $table->string('lastname', 64);
            $table->string('country_code', 3);
            $table->unsignedInteger('user_profile_id');   

            $table->timestamp('email_verified_at')->nullable();
            $table->rememberToken();

            $table->foreign('user_profile_id')->references('id')->on('user_profiles');
            $table->foreign('country_code')->references('code')->on('countries');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
