angular
  .module('app', ['ui.router', 'ui.router.state.events', 'ngStorage', 'toaster', 'ngAnimate', 'ui.bootstrap', 'pascalprecht.translate', 'ngSanitize']);
