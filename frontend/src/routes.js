angular
  .module('app')
  .config(httpInterceptorConfig)
  .config(routesConfig)
  .run(aclConfig);

function aclConfig($rootScope, $location, $state, $localStorage) {
    function getMeOutOfHere() {
        // your fallback logic here
    }
    
    $rootScope.$on('$stateChangeError', function() {
        getMeOutOfHere();
    });
    $rootScope.$on('$stateChangeStart', function(e, toState, toParams, fromState, fromParams) {

      var deny = false;

      if ($localStorage.alc !== null && toState.acl !== undefined) {
        deny = toState.acl.filter(function(n) {
          return $localStorage.acl.includes(n);
        }).length == 0;
      }

      if (deny) {
        e.preventDefault();
        $state.go('forbbiden');
      }

    });
}

function httpInterceptorConfig($provide, $httpProvider) {
  // register the interceptor as a service
  $provide.factory('myHttpInterceptor', function($q, $state, $localStorage, toaster) {
    return {
      request: function(config) {
        if ($localStorage.access_token !== null) {
          config.headers.authorization = $localStorage.token_type + ' ' + $localStorage.access_token;
        }
        return config;
      },
      responseError: function(rejection) {
        if (rejection.status == 401) {
          $localStorage.access_token = null;
          $localStorage.token_type = null;
          $state.go('login');
        } else if (rejection.status == 403) {
          //console.log(rejection.data.message);
          toaster.pop('error', "Error de Permisos!", rejection.data.message);

        } else if (rejection.status == 422) {
          errors = Object.keys(rejection.data.errors);
          errors.forEach(function(error) {
            aux = rejection.data.errors[error];
            toaster.pop('error', "Dato Invalido!", aux[0]);
          });

        } else if (rejection.status == 500) {
          toaster.pop('error', "Error de Servidor!", "Ocurrio un error con el servidor, vuelva a intentarlo mas tarde");
        }
        return $q.reject(rejection);
      }
    } 
  });

  //$httpProvider.defaults.withCredentials = true;
  $httpProvider.interceptors.push('myHttpInterceptor');  
}

/** @ngInject */
function routesConfig($stateProvider, $urlRouterProvider, $locationProvider) {
  $locationProvider.html5Mode(true).hashPrefix('!');
  $urlRouterProvider.otherwise('/');
  $stateProvider
    .state('layout', {
      abstract: true,
      url: '',
      templateUrl: 'app/layout/layout.html',
      controller: 'LayoutCtrl',
      controllerAs: 'vm',
    })
    .state('without-auth', {
      abstract: true,
      url: '',
      templateUrl: 'app/layout/without-auth.html'
    })
    .state('login', {
      url: '/',
      parent: 'without-auth',
      templateUrl: 'app/auth/login.html',
      controller: 'LoginCtrl',
      controllerAs: 'vm'
    })
    .state('registration', {
      url: '/new',
      parent: 'without-auth',
      templateUrl: 'app/auth/registration.html',
      controller: 'RegistrationCtrl',
      controllerAs: 'vm',
      resolve: {
        countries: function($http) {
          return $http.get('http://localhost:8000/api/countries')
            .then(function(response) {
              return response.data;
            });
        }
      }
    })
    .state('user-registration', {
      url: '/registration',
      parent: 'without-auth',
      templateUrl: 'app/users/edit.html',
      controller: 'UserEditCtrl',
      controllerAs: 'vm',
      resolve: {
        user: function() {
          return {};
        },
        countries: function($http) {
          return $http.get('http://localhost:8000/api/countries')
          .then(function(response) {
            return response.data;
          });
        },
        profiles: function($http) {
          return $http.get('http://localhost:8000/api/user-profiles/')
          .then(function(response) {
            return response.data;
          });
        }
      }
    })
    .state('forbbiden', {
      url: '/forbidden',
      parent: 'layout',
      templateUrl: 'app/layout/forbbiden.html'
    })
    .state('user-profile-list', {
      url: '/users-profile/list',
      parent: 'layout',
      templateUrl: 'app/user-profiles/list.html',
      controller: 'UserProfileListCtrl',
      controllerAs: 'vm',
      acl: ['user-profiles-full',  'user-profiles-read_only' ]
    })
    .state('user-profile-new', {
      url: '/users-profile/new',
      parent: 'layout',
      templateUrl: 'app/user-profiles/edit.html',
      controller: 'UserProfileEditCtrl',
      controllerAs: 'vm',
      acl:  ['user-profiles-full'],
      resolve: {
        modules: function($http) {
          return $http.get('http://localhost:8000/api/modules')
            .then(function(response) {
              return response.data;
            });
        },
        access_type: function($http, $stateParams) {
          return $http.get('http://localhost:8000/api/access-types')
            .then(function(response) {
              return response.data;
            });
        },
      	userProfile: function() {
      		return {};
      	}
      }
    })
    .state('user-profile-edit', {
      url: '/users-profile/edit/:id',
      parent: 'layout',
      templateUrl: 'app/user-profiles/edit.html',
      controller: 'UserProfileEditCtrl',
      controllerAs: 'vm',
      acl: ['user-profiles-full' ],
      resolve: {
        modules: function($http) {
          return $http.get('http://localhost:8000/api/modules')
            .then(function(response) {
              return response.data;
            });
        },
        access_type: function($http, $stateParams) {
          return $http.get('http://localhost:8000/api/access-types')
            .then(function(response) {
              return response.data;
            });
        },
      	userProfile: function($http, $stateParams, $localStorage) {
          return $http.get('http://localhost:8000/api/user-profiles/' + $stateParams.id)
          .then(function(response) {
					 return response.data;
				  });
      	}
      }
    })
    .state('user-list', {
      url: '/users/list',
      parent: 'layout',
      templateUrl: 'app/users/list.html',
      controller: 'UserListCtrl',
      controllerAs: 'vm',
      acl: ['users-full', 'users-read_only']
    })
    .state('user-edit', {
      url: '/users/edit/:id',
      parent: 'layout',
      templateUrl: 'app/users/edit.html',
      controller: 'UserEditCtrl',
      controllerAs: 'vm',
      acl: ['users-full'],
      resolve: {
      	user: function($http, $localStorage, $stateParams) {
           return $http.get('http://localhost:8000/api/user/' + $stateParams.id)
           .then(function(response) {
            console.log(response.data);
					 return response.data;
          });
      	},
        countries: function($http) {
          return $http.get('http://localhost:8000/api/countries')
            .then(function(response) {
              return response.data;
            });
        },
        profiles: function($http) {
          return $http.get('http://localhost:8000/api/user-profiles/')
            .then(function(response) {
              return response.data;
            });
        }
      }
    })
    .state('personal-info', {
      url: '/my-profile',
      parent: 'layout',
      templateUrl: 'app/personal-info/info.html',
      controller: 'InfoCtrl',
      controllerAs: 'vm',
      acl: ['users-full', 'users-read_only'],
    })
    .state('personal-info-edit', {
      url: '/my-profile/edit',
      parent: 'layout',
      templateUrl: 'app/personal-info/edit.html',
      controller: 'PersonalUserEditCtrl',
      controllerAs: 'vm',
      resolve: {
        countries: function($http) {
          return $http.get('http://localhost:8000/api/countries')
            .then(function(response) {
              return response.data;
            });
        }
      }
    })
    .state('message-panel', {
      url: '/messages',
      parent: 'layout',
      templateUrl: 'app/messages/messagePanel.html',
      controller: 'MessagePanelCtrl',
      controllerAs: 'vm',
    })
    .state('new-message', {
      url: '/messages/new',
      parent: 'layout',
      templateUrl: 'app/messages/newMessage.html',
      controller: 'newMessageCtrl',
      controllerAs: 'vm',
      resolve: {
        emails: function($http) {
          return $http.get('http://localhost:8000/api/emails')
            .then(function(response) {
              return response.data;
            });
        }
      }
    })
}
