(function() {
	angular
		.module('app')
		.controller('MessagePanelCtrl', function($http, $state, $localStorage, $scope) {
			var vm = this;

			vm.listar = function() {
				$http.get('http://localhost:8000/api/message')
				.then(function(response) {
							vm.messages = response.data.map(function(message){
								message.expand = false;
								return message;
							})
						}); 
			}

			vm.listar();

			vm.eliminar = function(id){
				$http.delete('http://localhost:8000/api/message/' + id)
				.then(function(response) {
							vm.listar();
							vm.getread('');
						});
			}

			vm.read = function(message){
				message.expand = !message.expand;
				message.read = true;
				vm.getread(message.id);
			}
			
			vm.getread = function(message_id){
				$http.get('http://localhost:8000/api/unread', {
					params: {
						message: message_id
					}
				})
				.then(function(response) {
					$localStorage.unread = response.data;
					$scope.$emit('message-read',{unread_messages: response.data});
				});
			}

			vm.getread('');
		});
})();
