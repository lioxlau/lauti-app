(function() {
	angular
		.module('app')
		.controller('newMessageCtrl', function($http, $state, emails, $localStorage, toaster, $translate) {
			var vm = this;
			vm.emails = emails;
			vm.recipient = '';

			vm.grabar = function(){
				vm.message.recipient_id = vm.recipient.id;
				$http.post('http://localhost:8000/api/message/', 
						vm.message)
					.then(function(response) {					
						$translate(['TOASTER.MESSAGE.TEXT1', 'TOASTER.MESSAGE.TEXT2', 'TOASTER.MESSAGE.LABEL']).then(function (text){
							toaster.pop('success', text['TOASTER.MESSAGE.LABEL'], text['TOASTER.MESSAGE.TEXT1'] + vm.message.subject +  text['TOASTER.MESSAGE.TEXT2'] + vm.recipient.email);
						});
						$state.go('message-panel');
					}, function(response) {
						console.log(response);
					});
			}
		});
})();
