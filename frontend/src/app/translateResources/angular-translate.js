(function() {
    angular
  	.module('app')
  	.config(['$translateProvider', function($translateProvider) {
		$translateProvider.registerAvailableLanguageKeys(['en', 'es']);
		$translateProvider.preferredLanguage('en');
		$translateProvider.useSanitizeValueStrategy('escape');
	}]);
})();
