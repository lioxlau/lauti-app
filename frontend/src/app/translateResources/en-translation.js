(function() {
    angular
  	.module('app')
  	.config(['$translateProvider', function($translateProvider) {
 		
 		var translations = {
		  	USERS_LIST: {
				HEADLINE: 'List of users',
				TABLE: {
					ID: 'ID',
					FIRSTNAME: 'First name',
					LASTNAME: 'Surname',
					EMAIL: 'E-mail',
					CITY: 'Location',
					COUNTRY: 'Country',
					USER_PROFILE: 'Profile',
					EDIT: 'Edit',
					DELETE: 'Delete',
					ATRIBUTES: 'Atributes',
					USERS: 'Users'
				},
				FILTER: {
					USER: {
						LABEL: 'User',
						PLACEHOLDER: 'Search by User'
					},
					COUNTRY: {
						LABEL: 'Country',
						PLACEHOLDER: 'Search by Country'
					},
					USER_PER_PAGE: 'Users per page',
					BUTTON: 'Filter',
					ERROR: 'No results found'
				}
			},
			USERS_EDIT: {
				HEADLINE: 'Edition of',
				FIRSTNAME: 'First name',
				LASTNAME: 'Surname',
				EMAIL: 'E-mail',
				CITY: 'Location',
				COUNTRY: 'Country',
				USER_PROFILE: 'Profile',
				BUTTON: 'Save'
			},
			USER_PROFILE_LIST: {
				HEADLINE: 'User profiles',
				TABLE: {
					ID: 'ID',
					NAME: 'Name',
					LEVEL: 'Level',
					USER: 'Users', 
					USER_PROFILE: 'Profiles',
					EDIT: 'Edit',
					DELETE: 'Delete',
					ATRIBUTES: 'Atributes',
					PROFILES: 'Profiles'
				},
				BUTTON: 'New'
			},
			USER_PROFILE_EDIT: {
				HEADLINE: 'Edition of',
				NAME: 'Profile Name',
				LEVEL: 'Profile Level',
				ACL: 'Profile Access',
				BUTTON: 'save'
			},
			USER_PROFILE_MODAL: {	
				HEADLINE: 'Delete profile',
				PER_USER: 'Apply by user',
				NEW_PROFILE: 'Select the profile to relocate users',
				SELECT: {
					VALUE: 'New profile',
					ERROR: 'Select a Profile'
				}
			},
			PERSONAL_INFO: {
				HEADLINE: 'Hello',
				BUTTON: 'Edit user'
			},
			MESSAGES_LIST: {
				HEADLINE: 'Inbox',
				SUBJECT: 'Affair',
				AUTHOR: 'Author',
				DATE: 'Date',
				DELETE: 'Delete',
				ID: 'ID',
				NEW: 'New',
				BUTTON: 'New'
			},
			MESSAGES_NEW: {
				HEADLINE: 'New message',
				SUBJECT: {
					LABEL: 'Affair',
					PLACEHOLDER: 'Enter the subject of the message'
				},
				RECIPIENT: {
					LABEL: 'Mail from the receiver',
					PLACEHOLDER: 'Choose the recipient of your message'
				},
				MESSAGE: {
					LABEL: 'Message',
					PLACEHOLDER: 'Write your message'
				},
				BUTTON: {
					RETURN: 'Return',
					SEND: 'Submit'	
				} 
			},
			LAYOUT: {
				USERS: 'Users',
				USER_PROFILES: 'User profiles',
				PROFILE: 'My profile',
				MESSAGES: 'Messages',
				LOGOUT: 'Logout',
				LANGUAGE: 'Language: '
			},
			LOGIN: {
				HEADLINE: 'Login',
				TEXT: {
					LABEL: 'If you do not have an account, ',
					REGISTER: 'Sign up'
				},
				POPUP: 'Error!',
				MAIL: {
					LABEL: 'Mail',
					PLACEHOLDER: 'Enter your mail'
				},
				PASSWORD: {
					LABEL: 'Password',
					PLACEHOLDER: 'Enter your password'
				},
				BUTTON: 'Enter',
				CHECK: 'Remember'
			},
			REGISTER: {
				HEADLINE: 'Registry',
				TEXT: {
					LABEL: 'If you already have an account, ',
					LOGIN: 'Enter'
				},
				EMAIL: 'E-mail',
				POPUP: 'Error!',
				PASSWORD: 'Password',
				PASSWORD_CONFIRMATION: 'Confirm Password',
				FIRSTNAME: 'First name',
				LASTNAME: 'Surname',
				CITY: 'Location',
				COUNTRY_CODE: 'Country',
				BUTTON: 'to register'
			},
			FORBIDEN: 'You do not have sufficient permissions',
			MODAL: {
				HEADLINE: 'Are you sure?',
				CONFIRM: 'Confirm',
				CANCEL: 'Cancel',
				TEXT: {
					USER_PROFILE: 'If this profile has users that use it, they will have to select a new profile to relocate them to',
					USERS: 'Are you sure you want to permanently delete this user?'
				}
			},
			FORM_UTILITES: {
				TEXT: 'You must enter ',
				EMAIL: 'Incorrect email address',
				VALUE: 'Select a value',
				SELECT: 'You must select '
			},
			TOASTER: {
				DELETE_USER: {
					LABEL: 'User Deleted!',
					TEXT: 'The user was successfully deleted'
				},
				EDIT_USER: {
					LABEL: 'Edited User!',
					TEXT: 'The user was successfully edited',
					ERROR_LABEL: 'Error in the user data!',
					ERROR_TEXT: 'The form has bug fixes to save'
				},
				DELETE_USER_PROFILE: {
					LABEL: 'Profile Deleted!',
					TEXT: 'The user profile was successfully deleted',
					ERROR_LABEL: 'Error deleting!',
					ERROR_TEXT: 'Users can not be transferred to another profile'
				},
				PROFILE_MODAL: {
					LABEL: 'Updated Users!',
					TEXT: 'The user profile can now be deleted'
				},
				MESSAGE: {
					LABEL: 'Message sent!',
					TEXT1: 'The message ',
					TEXT2: ' was sent to '
				}
			}
		};

		$translateProvider.translations('en', translations);
	}]);
})();