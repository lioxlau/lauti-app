(function() {
    angular
  	.module('app')
  	.config(['$translateProvider', function($translateProvider) {
 		
 		var translations = {
		  	USERS_LIST: {
				HEADLINE: 'Lista de usuarios',
				TABLE: {
					ID: 'ID',
					FIRSTNAME: 'Nombre',
					LASTNAME: 'Apellido',
					EMAIL: 'Email',
					CITY: 'Localidad',
					COUNTRY: 'Pais',
					USER_PROFILE: 'Perfil',
					EDIT: 'Editar',
					DELETE: 'Borrar',
					ATRIBUTES: 'Atributos',
					USERS: 'Usuarios'
				},
				FILTER: {
					USER: {
						LABEL: 'Usuario',
						PLACEHOLDER: 'Buscar por Usuario'
					},
					COUNTRY: {
						LABEL: 'Pais',
						PLACEHOLDER: 'Buscar por Pais'
					},
					USER_PER_PAGE: 'Usuarios por Pagina',
					BUTTON: 'Filtrar',
					ERROR: 'No se encontraron resultados'
				}
			},
			USERS_EDIT: {
				HEADLINE: 'Edicion de: ',
				FIRSTNAME: 'Nombre',
				LASTNAME: 'Apellido',
				EMAIL: 'Email',
				CITY: 'Localidad',
				COUNTRY: 'Pais',
				USER_PROFILE: 'Perfil',
				BUTTON: 'Guardar'
			},
			USER_PROFILE_LIST: {
				HEADLINE: 'Perfiles de usuarios',
				TABLE: {
					ID: 'ID',
					NAME: 'Nombre',
					LEVEL: 'Nivel',
					USER: 'Usuarios', 
					USER_PROFILE: 'Perfiles',
					EDIT: 'Editar',
					DELETE: 'Borrar',
					ATRIBUTES: 'Atributos',
					PROFILES: 'Perfiles'
				},
				BUTTON: 'Nuevo'
			},
			USER_PROFILE_EDIT: {
				HEADLINE: 'Edicion de ',
				NAME: 'Nombre del Perfil',
				LEVEL: 'Nivel del Perfil',
				ACL: 'Accesos del Perfil',
				BUTTON: 'Guardar'
			},
			USER_PROFILE_MODAL: {	
				HEADLINE: 'Borrar perfil',
				PER_USER: 'Aplicar por usuario',
				NEW_PROFILE: 'Selecciona el perfil a relocalizar usuarios: ',
				SELECT: {
					VALUE: 'Nuevo Perfil',
					ERROR: 'Seleccione un Perfil'
				}
			},
			PERSONAL_INFO: {
				HEADLINE: 'Hola ',
				BUTTON: 'Editar usuario'
			},
			MESSAGES_LIST: {
				HEADLINE: 'Bandeja de Entrada',
				SUBJECT: 'Asunto',
				AUTHOR: 'Autor',
				DATE: 'Fecha',
				DELETE: 'Borrar',
				ID: 'ID',
				NEW: 'Nuevo',
				BUTTON: 'Nuevo'
			},
			MESSAGES_NEW: {
				HEADLINE: 'Nuevo mensaje',
				SUBJECT: {
					LABEL: 'Asunto',
					PLACEHOLDER: 'Ingrese el asunto del mensaje'
				},
				RECIPIENT: {
					LABEL: 'Mail del receptor',
					PLACEHOLDER: 'Elija el receptor de su mensaje'
				},
				MESSAGE: {
					LABEL: 'Mensaje',
					PLACEHOLDER: 'Escriba su mensaje'
				},
				BUTTON: {
					RETURN: 'Volver',
					SEND: 'Enviar'	
				} 
			},
			LAYOUT: {
				USERS: 'Usuarios',
				USER_PROFILES: 'Perfiles de usuarios',
				PROFILE: 'Mi Perfil',
				MESSAGES: 'Mensajes',
				LOGOUT: 'Logout',
				LANGUAGE: 'Idioma: '
			},
			LOGIN: {
				HEADLINE: 'Login',
				TEXT: {
					LABEL: 'Si no tenes cuenta, ',
					REGISTER: 'registrate'
				},
				POPUP: 'Error!',
				MAIL: {
					LABEL: 'Mail',
					PLACEHOLDER: 'Ingrese su mail'
				},
				PASSWORD: {
					LABEL: 'Contrasenia',
					PLACEHOLDER: 'Ingrese su contrasenia'
				},
				BUTTON: 'Ingresar',
				CHECK: 'Recordar'
			},
			REGISTER: {
				HEADLINE: 'Registro',
				TEXT: {
					LABEL: 'Si ya tenes cuenta, ',
					LOGIN: 'ingresa'
				},
				EMAIL: 'Email',
				POPUP: 'Error!',
				PASSWORD: 'Contraseña',
				PASSWORD_CONFIRMATION: 'Confirmar Contraseña',
				FIRSTNAME: 'Nombre',
				LASTNAME: 'Apellido',
				CITY: 'Localidad',
				COUNTRY_CODE: 'Pais',
				BUTTON: 'Registrar'
			},
			FORBIDEN: 'No tienes permisos suficientes',
			MODAL: {
				HEADLINE: '¿Esta seguro?',
				CONFIRM: 'Confirmar',
				CANCEL: 'Cancelar',
				TEXT: {
					USER_PROFILE: 'Si este perfil tiene usuarios que lo utilizan va a tener que seleccionarle un nuevo perfil al cual reubicarlos',
					USERS: '¿Esta seguro de que quiere eliminar este usuario permanentemente?'
				}
			},
			FORM_UTILITES: {
				TEXT: 'Debe ingresar ',
				EMAIL: 'Direccion de mail incorrecta',
				VALUE: 'Seleccione un valor',
				SELECT: 'Debe seleccionar '
			},
			TOASTER: {
				DELETE_USER: {
					LABEL: 'Usuario Borrado!',
					TEXT: 'El usuario fue borrado con exito'
				},
				EDIT_USER: {
					LABEL: 'Usuario Editado!',
					TEXT: 'El usuario fue editado con exito',
					ERROR_LABEL: 'Error en los datos de Ususario!',
					ERROR_TEXT: 'El formulario tiene errores, arreglelos para guardar'
				},
				DELETE_USER_PROFILE: {
					LABEL: 'Perfil Borrado!',
					TEXT: 'El perfil de usuarios fue borrado con exito',
					ERROR_LABEL: 'Error al borrar!',
					ERROR_TEXT: 'Los usuarios no pueden ser pasados a otro perfil'
				},
				PROFILE_MODAL: {
					LABEL: 'Usuarios Actualizados!',
					TEXT: 'El perfil de usuarios ya se puede eliminar'
				},
				MESSAGE: {
					LABEL: 'Mensaje Enviado!',
					TEXT1: 'El mensaje ',
					TEXT2: ' fue enviado a '
				}
			}
		};

		$translateProvider.translations('es', translations);
	}]);
})();