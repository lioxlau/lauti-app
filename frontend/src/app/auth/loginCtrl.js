(function() {
	angular
		.module('app')
		.controller('LoginCtrl', function($http, $state, $localStorage) {
			var vm = this;
			vm.user = {};
			vm.user.remember_me = false;
			vm.errors = [];

			vm.login = function() {
				vm.errors = [];
				$http.post('http://localhost:8000/api/auth/login', vm.user)
					.then(function(response) {
						$localStorage.access_token = response.data.access_token;
						$localStorage.token_type = response.data.token_type;
						$localStorage.acl= response.data.acl;
						$localStorage.level= response.data.level;

						$http.get('http://localhost:8000/api/auth/user')
						.then(function(response) {
								$localStorage.user = response.data;
								$state.go('user-list');
							});
					}, function(response) {
						console.log(response);
						if(!response.data.errors) {
							vm.errors = [ response.data.message ];
						} else {
							console.log(response.data.errors);
							for (key in response.data.errors) {								
								Array.prototype.push.apply(vm.errors, response.data.errors[key]);
							}
						}
					});
			}

		});
})();
