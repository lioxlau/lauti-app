(function() {
	angular
		.module('app')
		.controller('RegistrationCtrl', function($http, $state, countries) {
			var vm = this;
			vm.user = {};
			vm.countries = countries;
			vm.errors = [];

			vm.regist = function(form) {
				vm.errors = [];
				if (form.$valid) {
					$http.post('http://localhost:8000/api/auth/signup', vm.user)
						.then(function(response) {
							$state.go('login');
						}, function(response) {
						console.log(response);
						if(!response.data.errors) {
							vm.errors = [ response.data.message ];
						} else {
							console.log(response.data.errors);
							for (key in response.data.errors) {								
								Array.prototype.push.apply(vm.errors, response.data.errors[key]);
							}}
						})
				}
			}

		});
})();
