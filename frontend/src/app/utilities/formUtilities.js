(function() {
	angular
		.module('app')
		.directive('lauinput', function() {
			return {
				require: '^form',
				restrict: 'E',
				scope: {
					id: '@',
					label: '@',
					model: '=',
					type: '@'

				},
				link: function(scope, element, attrs, controller) {
					scope.idform = controller[scope.id];
					scope.form = controller;
				},
				template: `
					<div class="form-group" ng-class="{ 'has-error': idform.$error.required && idform.$touched || idform.$error.email }">
						<label class="control-label" for="{{id}}">{{label}}</label>
						<input type="{{type}}" class="form-control" id="{{id}}" name="{{id}}" placeholder="{{label}}" ng-model="model" required>
						<div ng-show="form.$submitted || idform.$touched" class="help-block">
					      <div ng-show="idform.$error.required">{{'FORM_UTILITES.TEXT'|translate}} {{label}}.</div>
					      <div ng-show="idform.$error.email">{{'FORM_UTILITES.EMAIL'|translate}}.</div>
					    </div>
					</div>
			 	`,
			}
  		})
  	.directive('lauselect', function() {
			return {
				require: '^form',
				restrict: 'E',
				scope: {
					id: '@',
					label: '@',
					items: '=',
					value: '@',
					name: '@',
					model: '='

				},
				link: function(scope, element, attrs, controller) {
					scope.idform = controller[scope.id];
					scope.form = controller;
				},
				template: `
					<div class="form-group" ng-class="{ 'has-error': idform.$error.required && idform.$touched }">
						<label class="control-label" for="{{id}}">{{label}}</label>
						<select name="{{id}}"  id="{{id}}" class="form-control" ng-model="model" required>
				      		<option ng-repeat="item in items" ng-value="item[value]">{{item[name]}}</option>
				      		<option value="" disabled hidden ng-selected="model != null">{{'FORM_UTILITES.VALUE'|translate}}</option>
				    	</select>
						<div ng-show="form.$submitted || idform.$touched" class="help-block">
		   	 			 	<div ng-show="idform.$error.required">{{'FORM_UTILITES.SELECT'|translate}} {{label}}.</div>
		    			</div>
					</div>
			 	`,
			}
  		})
})();
/*
*/