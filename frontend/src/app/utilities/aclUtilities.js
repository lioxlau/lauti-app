(function() {
	angular
		.module('app')
		.directive('lauAclCheck', function($localStorage) {
			return {
				restrict: 'A',
				scope: {
					lauAclCheck: '='
				},				
				link: function(scope, element, attrs, controller, transcludeFn) {
					var hide = true;
					if ($localStorage.acl !== null) {
						hide = scope.lauAclCheck.filter(function(n) {
							return $localStorage.acl.includes(n);
						}).length == 0;
					}

					if (hide) {
						$(element).hide();
					}
				}
			}
  		})

  		.directive('lauLevelCheck', function($localStorage, checkAcl) {
			return {
				restrict: 'A',
				scope: {
					lauLevelCheck: '='
				},				
				link: function(scope, element, attrs, controller, transcludeFn) {
					if (checkAcl.checkLevel(scope.lauLevelCheck)) {
						$(element).show();
					} else {
						$(element).hide();
					}

				}
			}
  		})

		.service('checkAcl', function ($localStorage) {
		    var vm = this;
		    vm.checkLevel = function (level) {
		    	return level > $localStorage.level;
		 	};
		});
})();