(function() {
	angular
		.module('app')
		.controller('ConfirmCtrl', function ($uibModal) {	

			var vm = this;	

			vm.confirm = function (text, fn_true, id) {
			var confirmModal = $uibModal.open({
				animation: true,
				ariaLabelledBy: 'modal-title',
				ariaDescribedBy: 'modal-body',
				templateUrl: 'app/utilities/confirm-modal.html',
				controller: 'ConfirmModalCtrl',
				controllerAs: 'vm',
				size: 'md',
				resolve: {
			        text: function () {
			          return text;
			        }
			    }
			});

			confirmModal.result.then(function (bool) {
				if(bool){
					fn_true(id);
				}
			});
		};
	});

	angular
		.module('app')
		.controller('ConfirmModalCtrl', function ($uibModalInstance, text) {
		
		var vm = this;
		vm.text = text;

		vm.confirm = function () {
			$uibModalInstance.close(true);
		};

		vm.cancel = function () {
			$uibModalInstance.close(false);
		};
	});

})();