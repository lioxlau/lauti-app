(function() {
	angular
		.module('app')

		.directive("lautable", function() {
		  return {
		    restrict: "E",
		    scope: {
				sortingFn: '='
			},
		    controller: function($scope) {
		      this.sorting = function(field) {
				$scope.currentMethod = ($scope.currentField == field && $scope.currentMethod != 'desc' ? 'desc' : 'asc');
		      	$scope.currentField = field;

		        $scope.sortingFn($scope.currentField, $scope.currentMethod);
		      };
		      this.getCurrentField = function() {
		        return $scope.currentField;
		      };
		      this.getCurrentMethod = function() {
		        return $scope.currentMethod;
		      };
		    }
		  }
		})

		.directive("laucolum", function() {
		  return {
			restrict: "E",
			scope: {
				field: '@',
				name: '@'
			},
		    require: "^lautable",
		    transclude: true,
		    template: '<a href="">{{name}}</a><span class="glyphicon {{icon}} pull-right"></span>',

		    link: function (scope, element, attrs, lautableCtrl) {
				element.on("click", function() {
					lautableCtrl.sorting(scope.field);
				});

				scope.icon = 'glyphicon-sort';

				scope.$watchGroup([ lautableCtrl.getCurrentField, lautableCtrl.getCurrentMethod ], function() {

					scope.icon = 'glyphicon-sort';

					if (lautableCtrl.getCurrentField() == scope.field && lautableCtrl.getCurrentMethod() == 'asc') {
						scope.icon = 'glyphicon-sort-by-alphabet';

					} else if (lautableCtrl.getCurrentField() == scope.field && lautableCtrl.getCurrentMethod() == 'desc') {
						scope.icon = 'glyphicon-sort-by-alphabet-alt';
					}

				});
			}
		  }
		})

		.controller('TableCtrl', function(vm) {

			vm.getPages = function(max) {
			    var pages = [];
			    for (var i = 1; i <= max; i += 1) {
			        pages.push(i);
			    }
			    return pages;
			}

			vm.setPage = function(pag) {
				vm.filter_page = pag;
				vm.filtrar();
			}

			vm.nextPage = function() {
				if (vm.filter_page < vm.last_page) {
					vm.filter_page += 1;
					vm.filtrar();
				}
			}

			vm.previousPage = function() {
				if (vm.filter_page > 1) {
					vm.filter_page -= 1;
					vm.filtrar();
				}
			}

			vm.sorting = function(field, method) {
				vm.filter_sort_method = method;
				vm.filter_sort_field = field;
				vm.filtrar();
			}
		});

})();