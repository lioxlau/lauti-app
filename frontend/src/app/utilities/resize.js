(function() {
	angular
		.module('app')
		.directive('resize', ['$window', function ($window) {
			return {
				link: link,
				restrict: 'E',
			    scope: {
					fn: '='
				}
			};
			function link(scope, element, attrs){
				scope.width = $window.innerWidth;
				angular.element($window).bind('resize', function(){
					scope.width = $window.innerWidth;
					scope.$digest();
					if (scope.width < 1000) {
						scope.fn(true);
					} else {
						scope.fn(false);
					}
				});
			}
	 	}])
	 	.directive('compileTemplate', function($compile, $parse){
		    return {
		        link: function(scope, element, attr){
		            var parsed = $parse(attr.ngBindHtml);
		            function getStringValue() {
		                return (parsed(scope) || '').toString();
		            }
		            scope.$watch(getStringValue, function() {
		                $compile(element, null, -9999)(scope); 
		            });
		        }
		    }
		})
		.controller('resizeTableCtrl', function(vm, $sce, $templateRequest, $window, $timeout){
			vm.html = {};

			var table = $sce.getTrustedResourceUrl(vm.tablelarge);
            $templateRequest(table).then(function(template) {
				vm.tablehtml = $sce.trustAsHtml(template);
            });

			var smalltable = $sce.getTrustedResourceUrl(vm.tablesmall);
            $templateRequest(smalltable).then(function(template) {
				vm.tablesmallhtml = $sce.trustAsHtml(template);
            });            

			vm.resize = function(small_window) {
				$timeout(function() {
					if(small_window){
							vm.html = vm.tablesmallhtml;
					} else {
							vm.html = vm.tablehtml;
					}
				});
			}

			vm.resize($window.innerWidth < 1000);
		});
})();
