(function() {
	angular
		.module('app')
		.controller('UserProfileListCtrl', function ($uibModal, $controller, $log, $http, toaster, checkAcl, $translate) {

		var vm = this;
		vm.users = '';
		vm.text = 'MODAL.TEXT.USER_PROFILE';
		angular.extend(vm, $controller('ConfirmCtrl', {$uibModal: $uibModal}));
		vm.tablelarge = 'app/user-profiles/table.html';
		vm.tablesmall = 'app/user-profiles/table-small.html';
		
		angular.extend(vm, $controller('resizeTableCtrl', { vm: vm }));

		vm.listar = function() {
			$http.get('http://localhost:8000/api/user-profiles/')
				.then(function(response) {
					vm.userProfiles = response.data;
				}, function(response) {
					console.log(response);
				});
		};

		vm.listar();

		vm.eliminar = function(userProfileId) {
			$http.delete('http://localhost:8000/api/user-profiles/' + userProfileId)
				.then(function(response) {
						if(response.data.has_users){
							profiles = vm.userProfiles.filter(profile => 
								(profile.id != userProfileId && checkAcl.checkLevel(profile.level)));
							console.log(profiles.length);
							if (profiles.length >= 1) {
								vm.open(userProfileId, profiles);
							} else {
								$translate(['TOASTER.DELETE_USER_PROFILE.TEXT', 'TOASTER.DELETE_USER_PROFILE.LABEL']).then(function (text){
									toaster.pop('error', text['TOASTER.DELETE_USER_PROFILE.LABEL'], text['TOASTER.DELETE_USER_PROFILE.TEXT']);
								});
							}
						} else {
							$translate(['TOASTER.DELETE_USER_PROFILE.TEXT', 'TOASTER.DELETE_USER_PROFILE.LABEL']).then(function (text){
								toaster.pop('success', text['TOASTER.DELETE_USER_PROFILE.LABEL'], text['TOASTER.DELETE_USER_PROFILE.TEXT']);
							});
							vm.listar();
						}
					}, function(response) {
						console.log(response);
					});
		};

		vm.open = function (userProfileId, profiles) {
			var modalInstance = $uibModal.open({
			animation: true,
			ariaLabelledBy: 'modal-title',
			ariaDescribedBy: 'modal-body',
			templateUrl: 'app/user-profiles/modal/profile-modal.html',
			controller: 'ProfileModalCtrl',
			controllerAs: 'vm',
			size: 'md',
			resolve: {
				users: function(){ 
					return $http.get('http://localhost:8000/api/user-profiles/users/' + userProfileId)
		            .then(function(response) {
		            	return response.data;
		            });;
		        },
		        profiles: function() {
		        	return profiles;
		        },
		        userProfileId: function() {
		        	return userProfileId;
		        }
			}
			});

			modalInstance.result.then(function () {});
		};

		vm.auxconfirm = function(userProfile){
			vm.confirm(vm.text, vm.eliminar, userProfile.id);
		}
	});
})();