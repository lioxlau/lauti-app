(function() {
	angular
		.module('app')
		.controller('UserProfileAccessCtrl', function(
			$http, $state, profile, acl, modules, access_type) {

			var vm = this;
			vm.access_type = access_type;
			vm.modules = modules;
			vm.acl = acl; 
			vm.profile = profile; 

			savedModules = acl.map(function(t) { return t.module; });
			faltantes = modules.filter(function(module) {
				return savedModules.indexOf(module) == -1; 
			});

			faltantes.forEach(function(module) {
				vm.acl.push({ user_profile_id: profile.id, module: module, access_type: null });
			});

			console.log(faltantes);

			vm.guardar = function() {
				$http.post('http://localhost:8000/api/acl/' + profile.id, {
						acl: vm.acl
					}).then(function(response) {
						$state.go('user-profile-list');
					});
			}
		});
})();
