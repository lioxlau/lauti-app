(function() {
	angular
		.module('app')
		.controller('ProfileModalCtrl', function ($http, $uibModalInstance, users, profiles, userProfileId, toaster, $translate) {
		
		var vm = this;
		vm.users = users;
		vm.profiles = profiles;
		vm.onebyone = false;
		vm.userProfileId = userProfileId;
		vm.test = '';

		vm.eliminar = function() {
			$http.post('http://localhost:8000/api/user/update-profile', {
		    	users: vm.updateUsers
			})
			.then(function(response) {
					$translate(['TOASTER.PROFILE_MODAL.TEXT', 'TOASTER.PROFILE_MODAL.LABEL']).then(function (text){
						toaster.pop('success', text['TOASTER.PROFILE_MODAL.LABEL'], text['TOASTER.PROFILE_MODAL.TEXT']);
					});	
				}, function(response) {
					console.log(response);
				});
		};
		vm.confirm = function (form) {
			console.log(form.$valid)
			if(form.$valid) {
				fn = function(user){return vm.userProfileId};
				if(vm.onebyone){
					fn = function(user){return user.new_profile_id};
				}
				vm.updateUsers = vm.users.map(function(user){
					return user = {
						'id': user.id,
						'user_profile_id': fn(user)
					};
				})
				vm.eliminar()
				$uibModalInstance.close();
			}
		};

		vm.cancel = function () {
			$uibModalInstance.close();
		};
	});
})();