(function() {
	angular
		.module('app')
		.controller('UserProfileEditCtrl', function($http, $state, userProfile, modules, access_type) {
			var vm = this;
			vm.userProfile = userProfile;
			vm.profilename = vm.userProfile.name;
			vm.access_type = access_type;
			vm.modules = modules;

			vm.grabar = function(form) {
				if (form.$valid) {
					if (userProfile.id) {
						$http.put('http://localhost:8000/api/user-profiles/' + 
								vm.userProfile.id, vm.userProfile)
							.then(function(response) {
								$state.go('user-profile-list');
							}, function(response) {
								console.log(response);
							});

					} else {
						$http.post('http://localhost:8000/api/user-profiles/', 
								vm.userProfile)
							.then(function(response) {
								$state.go('user-profile-list');
							}, function(response) {
								console.log(response);
							});
					}
				}
			}

			vm.init = function(){
				if (!userProfile.id){
					vm.profilename = 'Nuevo Perfil';
					vm.userProfile.acls = [];
					vm.modules.forEach(function(module) {
						vm.userProfile.acls.push({
            				"module": module,
            				"access_type": "read_only",
						});
					})
				}
			}

			vm.init();
		});
})();