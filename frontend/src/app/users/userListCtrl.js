(function() {
	angular
		.module('app')
		.controller('UserListCtrl', function($scope, $http, $controller, toaster, $uibModal, $translate, $window) {

			var vm = this;

			vm.pages = [1];
			vm.filter_username = '';
			vm.filter_country_code = '';
			vm.filter_page = 1;
			vm.filter_paginate = 10;
			vm.last_page = 1;
			vm.filter_sort_field = '';
			vm.filter_sort_method = '';
			vm.text = 'MODAL.TEXT.USERS';
			vm.tablelarge = 'app/users/table.html';
			vm.tablesmall = 'app/users/table-small.html';


			angular.extend(vm, $controller('ConfirmCtrl', {$uibModal: $uibModal}));

			$http.get('http://localhost:8000/api/countries')
				.then(function(response) {
					vm.countries = response.data;
				});
			
			vm.filtrar = function() {
				$http.get('http://localhost:8000/api/user', {
							params: {
								firstname: vm.filter_username,
								country_code: vm.filter_country_code,
								page: vm.filter_page,
								per_page: vm.filter_paginate,
								sort_field: vm.filter_sort_field,
								sort_method: vm.filter_sort_method
							}
						}).then(function(response) {
							vm.users = response.data.data;
							vm.pages = vm.getPages(response.data.last_page);
							vm.last_page = response.data.last_page;
							vm.filter_country_code = '';
							vm.filter_page = 1;
						});
				$window.scrollTo(0, 0);
			}

			vm.filtrar();


			vm.eliminar = function(id) {
				$http.delete('http://localhost:8000/api/user/' + id)
				.then(function(response) {
						$translate(['TOASTER.DELETE_USER.TEXT', 'TOASTER.DELETE_USER.LABEL']).then(function (text){
							toaster.pop('success', text['TOASTER.DELETE_USER.LABEL'], text['TOASTER.DELETE_USER.TEXT']);
						});
						vm.filtrar();
					});
			}

			vm.auxconfirm = function(user){
				vm.confirm(vm.text, vm.eliminar, user.id);
			}

			angular.extend(vm, $controller('TableCtrl', { vm: vm }));
			angular.extend(vm, $controller('resizeTableCtrl', { vm: vm }));
		})
})();
