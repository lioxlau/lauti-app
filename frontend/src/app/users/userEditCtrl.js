(function() {
	angular
		.module('app')
		.controller('UserEditCtrl', function($http, $state, user, profiles, countries, toaster, $translate) {
			var vm = this;
			vm.user = user;
			vm.firstname = vm.user.firstname;
			vm.lastname = vm.user.lastname;
			vm.profiles = profiles; 
			vm.countries = countries;

			vm.grabar = function(form) {
				if (form.$valid) {
					if (user.id) {
						$http.put('http://localhost:8000/api/user/' + 
								vm.user.id, vm.user)
							.then(function(response) {
								$translate(['TOASTER.EDIT_USER.TEXT', 'TOASTER.EDIT_USER.LABEL']).then(function (text){
									toaster.pop('success', text['TOASTER.EDIT_USER.LABEL'], text['TOASTER.EDIT_USER.TEXT']);
								});
								$state.go('user-list');
							}, function(response) {
								console.log(response);
							});
					}
				} else {
					$translate(['TOASTER.EDIT_USER.ERROR_TEXT', 'TOASTER.EDIT_USER.ERROR_LABEL']).then(function (text){
						toaster.pop('error', text['TOASTER.EDIT_USER.ERROR_LABEL'], text['TOASTER.EDIT_USER.ERROR_TEXT']);
					});
				}
			}
		});
})();

