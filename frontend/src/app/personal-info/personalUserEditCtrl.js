(function() {
	angular
		.module('app')
		.controller('PersonalUserEditCtrl', function($http, $state, $localStorage, countries, toaster, $translate) {
			var vm = this;
			vm.user = $localStorage.user;
			vm.countries = countries;

			console.log($localStorage.user);

			vm.grabar = function() {
				
				vm.user.user_profile_id = null;

				$http.put('http://localhost:8000/api/user/' + vm.user.id, {
							id: vm.user.id,
							email: vm.user.email, 
							city: vm.user.city, 
							firstname: vm.user.firstname, 
							lastname: vm.user.lastname,
							country_code: vm.user.country_code
						})
					.then(function(response) {
						$state.go('personal-info');
						$translate(['TOASTER.EDIT_USER.TEXT', 'TOASTER.EDIT_USER.LABEL']).then(function (text){
							toaster.pop('success', text['TOASTER.EDIT_USER.LABEL'], text['TOASTER.EDIT_USER.TEXT']);
						});
					}, function(response) {
						console.log(response);
					});
			}
		});
})();
