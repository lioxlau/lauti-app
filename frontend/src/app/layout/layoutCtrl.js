(function() {
	angular
		.module('app')
		.controller('LayoutCtrl', function($http, $state, $localStorage, $rootScope, $scope, toaster, $translate) {
			var vm = this;
			vm.user = $localStorage.user;

			vm.logout = function() {
				$http.get('http://localhost:8000/api/auth/logout')
						.then(function(response) {
							$localStorage.access_token = null;
							$localStorage.token_type = null;
							$localStorage.user = null;
							$localStorage.acl = null;
							$localStorage.unread = null;
							$state.go('login');
						}, function(response) {
							console.log(response);
						});			
			}

			vm.getunread = function(){
				$http.get('http://localhost:8000/api/unread')
						.then(function(response) {
							vm.unread = response.data;
						});
			}

			vm.language = function(lang){
				if($translate.getAvailableLanguageKeys().includes(lang)){
					$translate.use(lang);
				}
			}

			vm.language(navigator.language.substring(0,2));
			vm.getunread();
			
			$scope.$on('message-read',function(event,data){
		    	vm.unread = data.unread_messages;
		  	});
		});

})();